import { Module } from '@nestjs/common';
import { DublinCoreTypesService } from './dublin-core-types.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DublinCoreType } from './entities/dublin-core-type.entity';
import { DublinCoreTypeTranslation } from './entities/dublin-core-type-translation.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([DublinCoreType, DublinCoreTypeTranslation]),
  ],
  providers: [DublinCoreTypesService],
  exports: [DublinCoreTypesService],
})
export class DublinCoreTypesModule {}
