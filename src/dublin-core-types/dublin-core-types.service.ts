import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { DublinCoreType } from './entities/dublin-core-type.entity';
import { FindOneOptions, Repository } from 'typeorm';
import { CreateDublinCoreTypeTranslationDto } from './dto/create-dublin-core-type-translation.dto';
import { DublinCoreTypeTranslation } from './entities/dublin-core-type-translation.entity';
import { CreateDublinCoreTypeDto } from './dto/create-dublin-core-type.dto';

@Injectable()
export class DublinCoreTypesService {
  constructor(
    @InjectRepository(DublinCoreType)
    private dublinCoreTypeRepository: Repository<DublinCoreType>,
    @InjectRepository(DublinCoreTypeTranslation)
    private dublinCoreTypeTranslationRepository: Repository<DublinCoreTypeTranslation>,
  ) {}

  count() {
    return this.dublinCoreTypeRepository.count();
  }

  create(createDublinCoreTypeDto: CreateDublinCoreTypeDto) {
    const dublinCoreType = new DublinCoreType(createDublinCoreTypeDto);
    return this.dublinCoreTypeRepository.save(dublinCoreType);
  }

  async createTranslation(
    createDublinCoreTypeTranslationDto: CreateDublinCoreTypeTranslationDto,
    dublinCoreType: DublinCoreType,
  ): Promise<DublinCoreTypeTranslation | null> {
    const dublinCoreTypeTranslation = new DublinCoreTypeTranslation(
      createDublinCoreTypeTranslationDto,
      dublinCoreType,
    );

    return this.dublinCoreTypeTranslationRepository.save(
      dublinCoreTypeTranslation,
    );
  }

  findOne(options?: FindOneOptions<DublinCoreType>) {
    return this.dublinCoreTypeRepository.findOne(options);
  }

  find() {
    return this.dublinCoreTypeRepository.find({
      relations: { dublinCoreTypeTranslations: true },
    });
  }
}
