import { Test, TestingModule } from '@nestjs/testing';
import { DublinCoreTypesService } from './dublin-core-types.service';

describe('DublinCoreTypesService', () => {
  let service: DublinCoreTypesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [DublinCoreTypesService],
    }).compile();

    service = module.get<DublinCoreTypesService>(DublinCoreTypesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
