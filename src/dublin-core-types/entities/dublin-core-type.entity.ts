import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { DublinCoreTypeTranslation } from './dublin-core-type-translation.entity';
import { CreateDublinCoreTypeDto } from '../dto/create-dublin-core-type.dto';
import { Item } from 'src/catalog/entities/item.entity';

@Entity()
export class DublinCoreType {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  image: string;

  @OneToMany(
    () => DublinCoreTypeTranslation,
    (dublinCoreTypeTranslation) => dublinCoreTypeTranslation.dublinCoreType,
  )
  dublinCoreTypeTranslations: DublinCoreTypeTranslation[];

  @OneToMany(() => Item, (item) => item.dublinCoreType)
  items: Item[];

  constructor(createDublinCoreTypeDto: CreateDublinCoreTypeDto) {
    if (!createDublinCoreTypeDto) return;
    this.image = createDublinCoreTypeDto.image;
  }
}
