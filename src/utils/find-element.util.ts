import { IOmekaElementText } from 'src/omeka/interfaces/omeka/omeka-element-text.interface';

export function findElement(
  elementId: number,
  elementTexts: IOmekaElementText[],
): string {
  const elementText = elementTexts.find(
    (elementText) => elementText.element.id === elementId,
  );
  return elementText?.text || '';
}
