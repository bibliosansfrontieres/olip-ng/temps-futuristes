import { Type } from 'class-transformer';
import { IsEnum, IsInt, IsOptional, Min } from 'class-validator';
import { OrderEnum } from './order.enum';

export class PageOptionsDto {
  @IsEnum(OrderEnum)
  @IsOptional()
  order?: OrderEnum;

  @Type(() => Number)
  @IsInt()
  @Min(1)
  @IsOptional()
  page?: number;

  @Type(() => Number)
  @IsInt()
  @Min(1)
  @IsOptional()
  take?: number;
}
