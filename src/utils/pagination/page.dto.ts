import { PageMetaDto } from './page-meta.dto';
import { PageOptions } from './page-options';

export class PageDto<T> {
  data: T[];

  meta: PageMetaDto;

  constructor(data: T[], totalItemsCount: number, pageOptions: PageOptions) {
    const meta = new PageMetaDto({ totalItemsCount, pageOptions });
    this.data = data;
    this.meta = meta;
  }
}
