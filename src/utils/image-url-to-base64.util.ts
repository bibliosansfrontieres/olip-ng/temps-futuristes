import axios from 'axios';

export async function imageUrlToBase64(imageUrl: string) {
  try {
    const extension = imageUrl.split('.').pop();
    const response = await axios.get(imageUrl, { responseType: 'arraybuffer' });
    const base64Image = Buffer.from(response.data, 'binary').toString('base64');
    return `data:image/${extension};base64,${base64Image}`;
  } catch (error) {
    console.error('Error converting image to Base64:', error.message);
    return null;
  }
}
