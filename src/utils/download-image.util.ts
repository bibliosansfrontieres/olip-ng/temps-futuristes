import axios from 'axios';
import { writeFileSync } from 'fs';
import { join } from 'path';
import { v4 as uuidv4 } from 'uuid';

export async function downloadImage(url: string, extension: string = 'png') {
  console.log('URL', url);
  try {
    const response = await axios.get<ArrayBuffer>(url, {
      responseType: 'arraybuffer',
    });
    const uuid = uuidv4();
    const filename = `${uuid}.${extension}`;
    const filePath = join(__dirname, '../../data/images', filename);

    writeFileSync(filePath, Buffer.from(response.data));

    return filename;
  } catch (error) {
    console.error('Error downloading image:', error.message);
  }
}
