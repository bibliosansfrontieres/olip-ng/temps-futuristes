import { Body, Controller, Get, Param, Post, Query } from '@nestjs/common';
import { CatalogService } from './catalog.service';
import { PageOptionsDto } from 'src/utils/pagination/page-options.dto';
import { GetCatalogOptionsDto } from './dto/get-catalog-options.dto';
import { UpdateOptionsDto } from './dto/update-options.dto';

@Controller('catalog')
export class CatalogController {
  constructor(private readonly catalogService: CatalogService) {}

  @Get()
  getCatalog(
    @Query() getCatalogOptionsDto: GetCatalogOptionsDto,
    @Query() pageOptionsDto: PageOptionsDto,
  ) {
    return this.catalogService.getCatalog(getCatalogOptionsDto, pageOptionsDto);
  }

  @Get('update')
  update(@Query('projectId') projectId?: string) {
    return this.catalogService.update(projectId);
  }

  @Get('update/categories')
  updateCategories() {
    return this.catalogService.updateCategories();
  }

  @Get(':id')
  getProject(@Param('id') id: string) {
    return this.catalogService.getProject(+id);
  }

  @Get('playlist/:id')
  getPlaylist(@Param('id') id: string) {
    return this.catalogService.getPlaylist(id);
  }

  @Post('check-updates')
  getUpdates(@Body() updateOptionsDto: UpdateOptionsDto) {
    return this.catalogService.checkUpdates(updateOptionsDto);
  }
}
