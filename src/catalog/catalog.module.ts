import { Module, forwardRef } from '@nestjs/common';
import { CatalogService } from './catalog.service';
import { CatalogController } from './catalog.controller';
import { OmekaModule } from 'src/omeka/omeka.module';
import { TransformerModule } from 'src/transformer/transformer.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Project } from './entities/project.entity';
import { Playlist } from './entities/playlist.entity';
import { PlaylistTranslation } from './entities/playlist-translation.entity';
import { Item } from './entities/item.entity';
import { ItemTranslation } from './entities/item-translation.entity';
import { ApplicationsModule } from 'src/applications/applications.module';
import { Category } from './entities/category.entity';
import { CategoryTranslation } from './entities/category-translation.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      Project,
      Playlist,
      PlaylistTranslation,
      Item,
      ItemTranslation,
      Category,
      CategoryTranslation,
    ]),
    forwardRef(() => OmekaModule),
    forwardRef(() => TransformerModule),
    ApplicationsModule,
  ],
  controllers: [CatalogController],
  providers: [CatalogService],
  exports: [CatalogService],
})
export class CatalogModule {}
