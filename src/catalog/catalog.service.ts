import { Inject, Injectable, forwardRef } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { OmekaCatalogService } from 'src/omeka/omeka-catalog.service';
import { OmekaTransformerService } from 'src/transformer/omeka-transformer.service';
import { Project } from './entities/project.entity';
import {
  FindManyOptions,
  FindOneOptions,
  In,
  Like,
  Not,
  Repository,
} from 'typeorm';
import { IOmekaItem } from 'src/omeka/interfaces/omeka/omeka-item.interface';
import { OmekaCheckerService } from 'src/omeka/omeka-checker.service';
import { IOmekaPackage } from 'src/omeka/interfaces/omeka/omeka-package.interface';
import { Playlist } from './entities/playlist.entity';
import { PlaylistTranslation } from './entities/playlist-translation.entity';
import { IOmekaFile } from 'src/omeka/interfaces/omeka/omeka-file.interface';
import { Item } from './entities/item.entity';
import { PageOptionsDto } from 'src/utils/pagination/page-options.dto';
import { PageOptions } from 'src/utils/pagination/page-options';
import { PageDto } from 'src/utils/pagination/page.dto';
import { ItemTranslation } from './entities/item-translation.entity';
import { GetCatalogOptionsDto } from './dto/get-catalog-options.dto';
import { UpdateOptionsDto } from './dto/update-options.dto';
import { ApplicationsService } from 'src/applications/applications.service';
import { OlipPlaylistDto } from 'src/transformer/dto/olip-playlist.dto';
import { OlipItemDto } from 'src/transformer/dto/olip-item.dto';
import { downloadImage } from 'src/utils/download-image.util';
import { IOmekaCollection } from 'src/omeka/interfaces/omeka/omeka-collection.interface';
import { Category } from './entities/category.entity';
import { CategoryTranslation } from './entities/category-translation.entity';
import * as crypto from 'crypto';

@Injectable()
export class CatalogService {
  constructor(
    @Inject(forwardRef(() => OmekaCatalogService))
    private omekaCatalogService: OmekaCatalogService,
    @Inject(forwardRef(() => OmekaTransformerService))
    private omekaTransformerService: OmekaTransformerService,
    private omekaCheckerService: OmekaCheckerService,
    @InjectRepository(Project)
    private projectsRepository: Repository<Project>,
    @InjectRepository(Category)
    private categoriesRepository: Repository<Category>,
    @InjectRepository(CategoryTranslation)
    private categoryTranslationsRepository: Repository<CategoryTranslation>,
    @InjectRepository(Playlist)
    private playlistsRepository: Repository<Playlist>,
    @InjectRepository(PlaylistTranslation)
    private playlistTranslationsRepository: Repository<PlaylistTranslation>,
    @InjectRepository(Item)
    private itemsRepository: Repository<Item>,
    @InjectRepository(ItemTranslation)
    private itemTranslationsRepository: Repository<ItemTranslation>,
    private applicationsService: ApplicationsService,
  ) {}

  onModuleInit() {
    this.checkApplicationsCatalogs();
    setInterval(this.checkApplicationsCatalogs.bind(this), 60 * 1000 * 10);
  }

  async checkApplicationsCatalogs() {
    // get each applications
    // get each catalog
    // put new contents in playlists database
    const applications = this.applicationsService.getApplications();
    for (const application of applications) {
      const catalogContents =
        await this.applicationsService.getApplicationCatalog(application.name);
      for (const catalogContent of catalogContents) {
        const playlist = await this.playlistsRepository.findOne({
          where: { id: catalogContent.id },
        });
        if (playlist) {
          // todo update playlist and items
        } else {
          console.log(`installing ${Date.now()}`);
          const thumbnail = `images/${await downloadImage(
            catalogContent.thumbnail,
          )}`;
          let playlist = new Playlist(
            {
              id: catalogContent.id,
              contentId: catalogContent.id,
              image: thumbnail,
              updatedAt: catalogContent.version,
              playlistTranslations: [],
              categoryIds: [],
              itemIds: [],
            } as OlipPlaylistDto,
            true,
            application.description.id,
          );
          playlist.size = catalogContent.size;
          playlist = await this.playlistsRepository.save(playlist);

          for (const translation of [
            {
              title: catalogContent.title,
              description: catalogContent.description,
              languageIdentifier: catalogContent.languageIdentifier,
            },
          ]) {
            const playlistTranslation = new PlaylistTranslation(
              translation,
              playlist,
            );
            await this.playlistTranslationsRepository.save(playlistTranslation);
          }
          //create item
          const olipItemDto: OlipItemDto = {
            id: playlist.id,
            thumbnail,
            createdCountry: '',
            createdAt: '',
            updatedAt: '',
            url: catalogContent.url,
            fileName: '',
            imageName: '',
            size: catalogContent.size,
            contentId: playlist.id,
            mimeType: catalogContent.mimeType,
            dublinCoreItem: {
              dublinCoreTypeId: 0,
              dublinCoreItemTranslations: [
                {
                  title: catalogContent.title,
                  description: catalogContent.description,
                  creator: '',
                  publisher: '',
                  source: '',
                  extent: '',
                  subject: '',
                  dateCreated: '',
                  languageIdentifier: catalogContent.languageIdentifier,
                },
              ],
            },
            itemDocumentType: {},
            itemLanguageLevel: {},
            file: {},
          } as OlipItemDto;
          const item = new Item(olipItemDto, true, application.description.id);
          await this.itemsRepository.save(item);
          const itemTranslation = new ItemTranslation(olipItemDto, item);
          await this.itemTranslationsRepository.save(itemTranslation);
          playlist.items = [item];
          await this.playlistsRepository.save(playlist);
        }
      }
    }
  }

  async getCatalog(
    getCatalogOptionsDto: GetCatalogOptionsDto,
    pageOptionsDto: PageOptionsDto,
  ) {
    const applicationIds = getCatalogOptionsDto?.applicationIds
      ? getCatalogOptionsDto.applicationIds.split(',')
      : undefined;
    const pageOptions = new PageOptions(pageOptionsDto);
    let where = undefined;
    if (getCatalogOptionsDto.query) {
      where = {
        playlistTranslations: {
          title: Like(`%${getCatalogOptionsDto.query}%`),
        },
        id: Not(In((getCatalogOptionsDto.playlistIds || '').split(','))),
        applicationId: applicationIds ? In(applicationIds) : undefined,
      };
    } else {
      where = {
        id: Not(In((getCatalogOptionsDto.playlistIds || '').split(','))),
        applicationId: applicationIds ? In(applicationIds) : undefined,
      };
    }
    const playlists = await this.playlistsRepository.find({
      where,
      relations: {
        items: { itemTranslations: true },
      },
      take: pageOptions.take,
      skip: pageOptions.skip,
      order: { id: pageOptions.order },
    });

    const totalPlaylists = await this.playlistsRepository.count({ where });

    for (const playlist of playlists) {
      let translation = null;
      if (getCatalogOptionsDto.languageIdentifier) {
        translation = await this.playlistTranslationsRepository.findOne({
          where: {
            playlist: { id: playlist.id },
            languageIdentifier: getCatalogOptionsDto.languageIdentifier,
          },
        });
      }
      if (translation === null) {
        translation = await this.playlistTranslationsRepository.findOne({
          where: {
            playlist: { id: playlist.id },
            languageIdentifier: 'eng',
          },
        });
      }

      if (translation === null) {
        translation = await this.playlistTranslationsRepository.findOne({
          where: { playlist: { id: playlist.id } },
        });
      }
      if (translation) {
        playlist.playlistTranslations = [translation];
      }
    }

    return new PageDto(playlists, totalPlaylists, pageOptions);
  }

  async getProject(id: number) {
    const project = await this.projectsRepository.findOne({
      where: { id },
      relations: {
        playlists: {
          playlistTranslations: true,
          items: { itemTranslations: true },
        },
      },
    });
    const categories = await this.categoriesRepository.find({
      relations: { categoryTranslations: true },
    });
    return { project, categories };
  }

  async update(projectId?: string) {
    if (projectId) {
      await this.saveCatalog(+projectId);
    } else {
      await this.saveCatalog();
    }
  }

  async updateCategories() {
    const collections = await this.omekaCatalogService.getOmekaCollections();

    for (const collection of collections) {
      await this.saveCollection(collection);
    }
  }

  async saveProject(catalogProject: IOmekaItem) {
    const errors = this.omekaCheckerService.checkOmekaProject(catalogProject);
    const isValid = errors.length === 0;
    const maestroProject =
      this.omekaTransformerService.transformProject(catalogProject);
    const project = new Project(maestroProject, isValid);
    await this.projectsRepository.save(project);
  }

  async savePlaylist(catalogPackage: IOmekaPackage, itemIds: number[]) {
    const errors = this.omekaCheckerService.checkOmekaPackage(catalogPackage);
    const isValid = errors.length === 0;
    const maestroPlaylist =
      await this.omekaTransformerService.transformPlaylist(catalogPackage);
    const playlist = new Playlist(maestroPlaylist, isValid, 'olip');
    await this.playlistsRepository.save(playlist);
    await this.playlistTranslationsRepository.delete({
      playlist: { id: playlist.id },
    });
    for (const omekaPackageTranslation of maestroPlaylist.playlistTranslations) {
      const playlistTranslation = new PlaylistTranslation(
        omekaPackageTranslation,
        playlist,
      );
      await this.playlistTranslationsRepository.save(playlistTranslation);
    }
  }

  async saveProjectRelation(omekaProject: IOmekaItem, omekaPackages: number[]) {
    const project = await this.projectsRepository.findOne({
      where: { id: omekaProject.id },
    });
    const playlists = await this.playlistsRepository.find({
      where: { id: In(omekaPackages) },
    });
    project.playlists = playlists;
    await this.projectsRepository.save(project);
  }

  async saveCollectionRelation(packageId: string, collectionName: string) {
    if (!collectionName) return;
    const playlist = await this.playlistsRepository.findOne({
      where: { id: packageId },
    });
    const category = await this.categoriesRepository.findOne({
      where: { categoryTranslations: { title: collectionName.toLowerCase() } },
    });
    if (!category) return;
    playlist.categoryId = category.id;
    await this.playlistsRepository.save(playlist);
  }

  async savePlaylistRelation(playlistId: string, itemIds: number[]) {
    const playlist = await this.playlistsRepository.findOne({
      where: { id: playlistId },
    });
    const items = await this.itemsRepository.find({
      where: { id: In(itemIds) },
    });
    playlist.items = items;
    await this.playlistsRepository.save(playlist);
  }

  async saveCollection(omekaCollection: IOmekaCollection) {
    const maestroCategory =
      this.omekaTransformerService.transformCollection(omekaCollection);
    const category = new Category(+maestroCategory.id);
    await this.categoriesRepository.save(category);
    await this.categoryTranslationsRepository.delete({
      category: { id: category.id },
    });
    // TODO : hard coded languageIdentifier
    const categoryTranslation = new CategoryTranslation(
      maestroCategory.title,
      'eng',
      category,
    );
    await this.categoryTranslationsRepository.save(categoryTranslation);
  }

  async saveItem(omekaItemAndFile: {
    omekaItem: IOmekaItem;
    omekaFile: IOmekaFile;
  }) {
    const errors = this.omekaCheckerService.checkOmekaItem(
      omekaItemAndFile.omekaItem,
    );
    const isValid = errors.length === 0;
    const maestroItem =
      this.omekaTransformerService.transformItem(omekaItemAndFile);
    const item = new Item(maestroItem, isValid, 'olip');
    await this.itemsRepository.save(item);
    await this.itemTranslationsRepository.delete({
      item: { id: item.id },
    });
    const itemTranslation = new ItemTranslation(maestroItem, item);
    await this.itemTranslationsRepository.save(itemTranslation);
  }

  async deleteCatalog() {
    await this.playlistsRepository.delete({ applicationId: null });
    await this.projectsRepository.delete({});
  }

  async saveCatalog(projectId?: number) {
    const catalog = await this.omekaCatalogService.getOmekaCatalog(projectId);

    for (const collection of catalog.omekaCollections) {
      console.log(`save collection ${collection.id}`);
      await this.saveCollection(collection);
    }

    for (const catalogItem of catalog.omekaItemsAndFiles) {
      console.log(`save item ${catalogItem.omekaItem.id}`);
      await this.saveItem(catalogItem);
    }

    for (const catalogProject of catalog.omekaProjects) {
      console.log(`save project ${catalogProject.id}`);
      await this.saveProject(catalogProject);
    }

    for (const catalogPackage of catalog.omekaPackages) {
      const { itemIds } = catalog.omekaPackagesItems.find(
        (value) => value.packageId === catalogPackage.id,
      );
      console.log(`save playlist ${catalogPackage.id}`);
      await this.savePlaylist(catalogPackage, itemIds);
      await this.saveCollectionRelation(
        catalogPackage.id.toString(),
        catalogPackage.subject,
      );
    }

    for (let i = 0; i < catalog.omekaProjects.length; i++) {
      const omekaProject = catalog.omekaProjects[i];
      const omekaPackages = catalog.omekaProjectsPackages[i];
      console.log(
        `save project relation ${omekaProject.id} -> ${omekaPackages.length}`,
      );
      await this.saveProjectRelation(omekaProject, omekaPackages);
    }

    for (let i = 0; i < catalog.omekaPackagesItems.length; i++) {
      const { packageId, itemIds } = catalog.omekaPackagesItems[i];
      console.log(`save playlist relations ${packageId} -> ${itemIds.length}`);
      await this.savePlaylistRelation(packageId.toString(), itemIds);
    }

    console.log('update playlists size');
    await this.updatePlaylistsSize(projectId);

    console.log('update projects version');
    await this.updateProjectsVersion(projectId);
  }

  async updateProjectsVersion(projectId?: number) {
    const projects = await this.projectsRepository.find({
      where: { id: projectId },
      relations: {
        playlists: { items: true },
      },
    });
    for (const project of projects) {
      const playlistVersions = [];
      for (const playlist of project.playlists) {
        const itemVersions = [];
        for (const item of playlist.items) {
          itemVersions.push(item.updatedAt);
        }
        const md5Items = crypto
          .createHash('md5')
          .update(`${playlist.updatedAt}${itemVersions.join('')}`)
          .digest('hex');
        playlistVersions.push(md5Items);
      }
      const md5Project = crypto
        .createHash('md5')
        .update(playlistVersions.join(''))
        .digest('hex');
      project.version = md5Project;
      await this.projectsRepository.save(project);
    }
  }

  async updatePlaylistsSize(projectId?: number) {
    const where = projectId ? { projects: { id: projectId } } : undefined;
    const playlists = await this.playlistsRepository.find({
      relations: { items: true },
      where,
    });

    for (const playlist of playlists) {
      let size = 0;
      for (const item of playlist.items) {
        size += item.size;
      }
      playlist.size = size;
      await this.playlistsRepository.save(playlist);
    }
  }

  async getPlaylist(id: string) {
    const playlist = await this.playlistsRepository.findOne({
      where: { id },
      relations: {
        playlistTranslations: true,
        items: { itemTranslations: true, dublinCoreType: true },
      },
    });
    console.log(playlist);
    playlist.items = playlist.items.map((item) => {
      // @ts-expect-error i need to change type
      item.dublinCoreItem = {
        dublinCoreItemTranslations: item.itemTranslations,
      };
      return item;
    });
    return playlist;
  }

  async checkUpdates(updateOptionsDto: UpdateOptionsDto) {
    console.log(updateOptionsDto);
    const playlists = await this.playlistsRepository.find({
      where: {
        id: In(updateOptionsDto.playlists.map((playlist) => playlist.id)),
      },
    });
    const items = await this.itemsRepository.find({
      where: { id: In(updateOptionsDto.items.map((item) => item.id)) },
    });
    console.log(items);
    const applications = this.applicationsService.getApplications();
    const itemsToUpdate = [];
    const playlistsToUpdate = [];
    const applicationsToUpdate = [];
    for (const item of items) {
      const olipItem = updateOptionsDto.items.find(
        (i) => `${i.id}` === item.id,
      );
      if (item.updatedAt !== olipItem.version) {
        itemsToUpdate.push(item.id);
      }
    }
    for (const playlist of playlists) {
      const olipPlaylist = updateOptionsDto.playlists.find(
        (p) => `${p.id}` === playlist.id,
      );
      if (playlist.updatedAt !== olipPlaylist.version) {
        playlistsToUpdate.push(playlist.id);
      }
    }
    for (const application of applications) {
      const olipApplication = updateOptionsDto.applications.find(
        (a) => a.id === application.description.id,
      );
      if (!olipApplication) continue;
      if (application.description.version !== olipApplication.version) {
        applicationsToUpdate.push(application.description.id);
      }
    }
    return {
      playlistIds: playlistsToUpdate,
      itemIds: itemsToUpdate,
      applicationIds: applicationsToUpdate,
    };
  }

  async findOneCategory(options?: FindOneOptions<Category>) {
    return this.categoriesRepository.findOne(options);
  }

  async findAllCategories(options?: FindManyOptions<Category>) {
    return this.categoriesRepository.find(options);
  }
}
