import {
  Column,
  Entity,
  JoinTable,
  ManyToMany,
  OneToMany,
  PrimaryColumn,
} from 'typeorm';
import { Project } from './project.entity';
import { OlipPlaylistDto } from 'src/transformer/dto/olip-playlist.dto';
import { PlaylistTranslation } from './playlist-translation.entity';
import { Item } from './item.entity';

@Entity()
export class Playlist {
  @PrimaryColumn()
  id: string;

  @Column()
  image: string;

  @Column()
  updatedAt: string;

  @Column()
  isValid: boolean;

  @Column()
  size: number;

  @Column({ nullable: true })
  categoryId: number;

  @Column({ nullable: true })
  applicationId: string | null;

  @ManyToMany(() => Project, (project) => project.playlists)
  projects: Project[];

  @OneToMany(() => PlaylistTranslation, (translation) => translation.playlist)
  playlistTranslations: PlaylistTranslation[];

  @ManyToMany(() => Item, (item) => item.playlists, { cascade: true })
  @JoinTable()
  items: Item[];

  constructor(
    omekaPackage: OlipPlaylistDto,
    isValid: boolean,
    applicationId: string | null = null,
  ) {
    if (!omekaPackage) return;
    this.id = omekaPackage.id.toString();
    this.image = omekaPackage.image;
    this.updatedAt = omekaPackage.updatedAt;
    this.isValid = isValid;
    this.applicationId = applicationId;
    this.size = 0;
  }
}
