import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { OlipItemDto } from 'src/transformer/dto/olip-item.dto';
import { Item } from './item.entity';

@Entity()
export class ItemTranslation {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  title: string;

  @Column()
  description: string;

  @Column()
  creator: string;

  @Column()
  publisher: string;

  @Column()
  source: string;

  @Column()
  extent: string;

  @Column()
  subject: string;

  @Column()
  dateCreated: string;

  @ManyToOne(() => Item, (item) => item.itemTranslations, {
    onDelete: 'CASCADE',
  })
  item: Item;

  constructor(olipItemDto: OlipItemDto, item: Item) {
    if (!olipItemDto) return;
    this.title = olipItemDto.dublinCoreItem.dublinCoreItemTranslations[0].title;
    this.description =
      olipItemDto.dublinCoreItem.dublinCoreItemTranslations[0].description;
    this.creator =
      olipItemDto.dublinCoreItem.dublinCoreItemTranslations[0].creator;
    this.publisher =
      olipItemDto.dublinCoreItem.dublinCoreItemTranslations[0].publisher;
    this.source =
      olipItemDto.dublinCoreItem.dublinCoreItemTranslations[0].source;
    this.extent =
      olipItemDto.dublinCoreItem.dublinCoreItemTranslations[0].extent;
    this.subject =
      olipItemDto.dublinCoreItem.dublinCoreItemTranslations[0].subject;
    this.dateCreated =
      olipItemDto.dublinCoreItem.dublinCoreItemTranslations[0].dateCreated;
    this.item = item;
  }
}
