import { OlipPlaylistTranslationDto } from 'src/transformer/dto/olip-playlist-translation.dto';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Playlist } from './playlist.entity';

@Entity()
export class PlaylistTranslation {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  title: string;

  @Column()
  description: string;

  @Column()
  languageIdentifier: string;

  @ManyToOne(() => Playlist, (playlist) => playlist.playlistTranslations, {
    onDelete: 'CASCADE',
  })
  playlist: Playlist;

  constructor(
    olipPlaylistTranslationDto: OlipPlaylistTranslationDto,
    playlist: Playlist,
  ) {
    if (!olipPlaylistTranslationDto) return;
    this.title = olipPlaylistTranslationDto.title;
    this.description = olipPlaylistTranslationDto.description;
    this.languageIdentifier = olipPlaylistTranslationDto.languageIdentifier;
    this.playlist = playlist;
  }
}
