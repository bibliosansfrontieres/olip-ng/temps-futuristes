import { OlipProjectDto } from 'src/transformer/dto/olip-project.dto';
import { Column, Entity, JoinTable, ManyToMany, PrimaryColumn } from 'typeorm';
import { Playlist } from './playlist.entity';

@Entity()
export class Project {
  @PrimaryColumn({ unique: true })
  id: number;

  @Column()
  url: string;

  @Column()
  title: string;

  @Column({ nullable: true })
  version: string | null;

  @Column()
  description: string;

  @Column()
  date: string;

  @Column()
  languages: string;

  @Column()
  startDate: string;

  @Column()
  endDate: string;

  @Column()
  partners: string;

  @Column()
  location: string;

  @Column()
  device: string;

  @Column()
  projectManager: string;

  @Column()
  isValid: boolean;

  @ManyToMany(() => Playlist, (playlist) => playlist.projects)
  @JoinTable()
  playlists: Playlist[];

  constructor(maestroProject: OlipProjectDto, isValid: boolean) {
    if (!maestroProject) return;
    this.id = maestroProject.id;
    this.url = maestroProject.url;
    this.title = maestroProject.title;
    this.description = maestroProject.description;
    this.date = maestroProject.date;
    this.languages = maestroProject.languages;
    this.startDate = maestroProject.startDate;
    this.endDate = maestroProject.endDate;
    this.partners = maestroProject.partners;
    this.location = maestroProject.location;
    this.device = maestroProject.device;
    this.projectManager = maestroProject.projectManager;
    this.isValid = isValid;
  }
}
