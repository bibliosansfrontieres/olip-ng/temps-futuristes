import {
  Column,
  Entity,
  ManyToMany,
  ManyToOne,
  OneToMany,
  PrimaryColumn,
} from 'typeorm';
import { OlipItemDto } from 'src/transformer/dto/olip-item.dto';
import { DublinCoreType } from 'src/dublin-core-types/entities/dublin-core-type.entity';
import { ItemTranslation } from './item-translation.entity';
import { Playlist } from './playlist.entity';

@Entity()
export class Item {
  @PrimaryColumn()
  id: string;

  @Column({ nullable: true })
  thumbnail: string | null;

  @Column()
  createdCountry: string;

  @Column()
  createdAt: string;

  @Column()
  updatedAt: string;

  @Column({ nullable: true })
  url: string | null;

  @Column()
  fileName: string;

  @Column()
  imageName: string;

  @Column()
  size: number;

  @Column()
  mimeType: string;

  @Column()
  isValid: boolean;

  @ManyToOne(() => DublinCoreType, (dublinCoreType) => dublinCoreType.items)
  dublinCoreType: DublinCoreType;

  @OneToMany(() => ItemTranslation, (itemTranslation) => itemTranslation.item)
  itemTranslations: ItemTranslation[];

  @ManyToMany(() => Playlist, (playlist) => playlist.items)
  playlists: Playlist[];

  @Column()
  applicationId: string;

  constructor(
    olipItemDto: OlipItemDto,
    isValid: boolean,
    applicationId: string,
  ) {
    if (!olipItemDto) return;
    this.id = olipItemDto.id.toString();
    this.thumbnail = olipItemDto.thumbnail;
    this.url = olipItemDto.url;
    this.fileName = olipItemDto.fileName;
    this.imageName = olipItemDto.imageName;
    this.size = olipItemDto.size;
    this.mimeType = olipItemDto.mimeType;
    this.createdAt = olipItemDto.createdAt;
    this.updatedAt = olipItemDto.updatedAt;
    this.createdCountry = olipItemDto.createdCountry;
    this.isValid = isValid;
    this.applicationId = applicationId;
  }
}
