import { Entity, OneToMany, PrimaryColumn } from 'typeorm';
import { CategoryTranslation } from './category-translation.entity';

@Entity()
export class Category {
  @PrimaryColumn()
  id: number;

  @OneToMany(
    () => CategoryTranslation,
    (categoryTranslation) => categoryTranslation.category,
  )
  categoryTranslations: CategoryTranslation[];

  constructor(id: number) {
    if (!id) return;
    this.id = id;
  }
}
