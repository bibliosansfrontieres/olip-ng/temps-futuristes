import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Category } from './category.entity';

@Entity()
export class CategoryTranslation {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  title: string;

  @Column()
  languageIdentifier: string;

  @ManyToOne(() => Category, (category) => category.categoryTranslations)
  category: Category;

  constructor(title: string, languageIdentifier: string, category: Category) {
    if (!title || !category) return;
    this.title = title;
    this.category = category;
    this.languageIdentifier = languageIdentifier;
  }
}
