import { IsOptional, IsString } from 'class-validator';

export class GetCatalogOptionsDto {
  @IsString()
  languageIdentifier: string;

  @IsString()
  @IsOptional()
  query?: string;

  @IsString()
  playlistIds?: string;

  @IsString()
  applicationIds: string;
}
