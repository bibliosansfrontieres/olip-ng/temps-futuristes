import { IsArray } from 'class-validator';

export class UpdateOptionsDto {
  @IsArray()
  playlists: { id: string; version: string }[];

  @IsArray()
  items: { id: string; version: string }[];

  @IsArray()
  applications: { id: string; version: string }[];
}
