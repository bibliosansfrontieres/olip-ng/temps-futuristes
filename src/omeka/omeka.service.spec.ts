import { Test, TestingModule } from '@nestjs/testing';
import { OmekaService } from './omeka.service';

describe('OmekaService', () => {
  let service: OmekaService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [OmekaService],
    }).compile();

    service = module.get<OmekaService>(OmekaService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
