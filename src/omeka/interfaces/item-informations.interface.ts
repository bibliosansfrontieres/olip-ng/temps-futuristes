import { ItemTypesEnum } from '../enums/item-types.enum';

export interface IItemInformations {
  id: number;
  url: string;
  itemType: ItemTypesEnum;
  collection: string;
  owner: string;
  file: string;
  tags: string[];
  title: string;
  description: string;
  creator: string;
  publisher: string;
  dateCreated: string;
  language: string;
  source: string;
  duration: string;
  size: string;
  audience: string;
  subject: string;
  format: string;
  type: string;
}
