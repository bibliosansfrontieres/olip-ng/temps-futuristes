import { IMarketplaceDublinCoreItem } from './marketplace-dublin-core-item.interface';
import { IMarketplaceFile } from './marketplace-file.interface';
import { IMarketplaceItemDocumentType } from './marketplace-item-document-type.interface';
import { IMarketplaceItemLanguageLevel } from './marketplace-item-language-level.interface';

export interface IMarketplaceItem {
  thumbnail: string;
  createdCountry: string;
  isInstalled: boolean;
  isUpdateNeeded: boolean;
  isBroken: boolean;
  isManuallyInstalled: boolean;
  version: string | null;
  contentId: string | null;
  dublinCoreItem: IMarketplaceDublinCoreItem;
  itemDocumentType?: IMarketplaceItemDocumentType;
  itemLanguageLevel?: IMarketplaceItemLanguageLevel;
  file: IMarketplaceFile;
}
