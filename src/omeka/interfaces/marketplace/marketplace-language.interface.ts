export interface IMarketplaceLanguage {
  id: number;
  identifier: string;
  oldIdentifier: string | null;
  label: string;
}
