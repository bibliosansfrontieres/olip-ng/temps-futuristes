import { IMarketplaceLanguage } from './marketplace-language.interface';

export interface IMarketplaceItemDocumentTypeTranslation {
  id: number;
  label: string;
  description: string;
  language: IMarketplaceLanguage;
}
