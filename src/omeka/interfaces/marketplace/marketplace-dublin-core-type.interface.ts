import { IMarketplaceDublinCoreTypeTranslation } from './marketplace-dublin-core-type-translation.interface';

export interface IMarketplaceDublinCoreType {
  id: number;
  image: string;
  dublinCoreTypeTranslations: IMarketplaceDublinCoreTypeTranslation[];
}
