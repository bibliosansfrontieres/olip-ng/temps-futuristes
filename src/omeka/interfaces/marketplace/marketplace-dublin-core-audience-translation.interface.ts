import { IMarketplaceLanguage } from './marketplace-language.interface';

export interface IMarketplaceDublinCoreAudienceTranslation {
  id: number;
  label: string;
  description: string;
  language: IMarketplaceLanguage;
}
