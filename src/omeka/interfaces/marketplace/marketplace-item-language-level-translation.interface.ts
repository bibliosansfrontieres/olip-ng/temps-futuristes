import { IMarketplaceLanguage } from './marketplace-language.interface';

export interface IMarketplaceItemLanguageLevelTranslation {
  id: number;
  label: string;
  description: string;
  language: IMarketplaceLanguage;
}
