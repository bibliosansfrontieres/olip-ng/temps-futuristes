export interface IMarketplacePlaylistTranslation {
  title: string;
  description: string;
  languageIdentifier: string;
}
