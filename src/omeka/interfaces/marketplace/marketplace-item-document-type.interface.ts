import { IMarketplaceItemDocumentTypeTranslation } from './marketplace-item-document-type-translation.interface';

export interface IMarketplaceItemDocumentType {
  id: number;
  itemDocumentTypeTranslations: IMarketplaceItemDocumentTypeTranslation[];
}
