import { IMarketplaceDublinCoreAudienceTranslation } from './marketplace-dublin-core-audience-translation.interface';

export interface IMarketplaceDublinCoreAudience {
  id: number;
  dublinCoreAudienceTranslations: IMarketplaceDublinCoreAudienceTranslation[];
}
