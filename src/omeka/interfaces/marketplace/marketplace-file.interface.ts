export interface IMarketplaceFile {
  url: string;
  size: string;
  duration: string;
  pages: string;
  extension: string;
  mimeType: string;
}
