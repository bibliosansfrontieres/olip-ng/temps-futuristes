import { MarketplaceLanguageTypesEnum } from 'src/omeka/enums/marketplace/marketplace-language-types.enum';
import { IMarketplaceLanguage } from './marketplace-language.interface';

export interface IMarketplaceDublinCoreLanguage {
  id: number;
  type: MarketplaceLanguageTypesEnum;
  language: IMarketplaceLanguage;
}
