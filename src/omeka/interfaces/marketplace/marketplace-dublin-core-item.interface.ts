import { IMarketplaceDublinCoreAudience } from './marketplace-dublin-core-audience.interface';
import { IMarketplaceDublinCoreItemTranslation } from './marketplace-dublin-core-item-translation.interface';
import { IMarketplaceDublinCoreLanguage } from './marketplace-dublin-core-language.interface';
import { IMarketplaceDublinCoreLicense } from './marketplace-dublin-core-license.interface';

export interface IMarketplaceDublinCoreItem {
  dublinCoreLanguages: IMarketplaceDublinCoreLanguage[];
  dublinCoreLicenses: IMarketplaceDublinCoreLicense[];
  dublinCoreAudiences: IMarketplaceDublinCoreAudience[];
  dublinCoreTypeId: number;
  dublinCoreItemTranslations: IMarketplaceDublinCoreItemTranslation[];
}
