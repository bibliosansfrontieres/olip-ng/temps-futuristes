import { IMarketplaceItemLanguageLevelTranslation } from './marketplace-item-language-level-translation.interface';

export interface IMarketplaceItemLanguageLevel {
  id: number;
  itemLanguageLevelTranslations: IMarketplaceItemLanguageLevelTranslation[];
}
