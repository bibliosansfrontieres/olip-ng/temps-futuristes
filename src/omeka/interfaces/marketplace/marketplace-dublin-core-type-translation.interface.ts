export interface IMarketplaceDublinCoreTypeTranslation {
  id: number;
  label: string;
  description: string;
  languageIdentifier: string;
}
