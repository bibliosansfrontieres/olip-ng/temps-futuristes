import { IMarketplaceDublinCoreLicenseTranslation } from './marketplace-dublin-core-license-translation.interface';

export interface IMarketplaceDublinCoreLicense {
  id: number;
  dublinCoreLicenseTranslations: IMarketplaceDublinCoreLicenseTranslation[];
}
