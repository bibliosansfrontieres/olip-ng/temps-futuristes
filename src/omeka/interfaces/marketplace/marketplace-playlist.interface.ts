import { IMarketplaceItem } from './marketplace-item.interface';
import { IMarketplacePlaylistTranslation } from './marketplace-playlist-translation.interface';

export interface IMarketplacePlaylist {
  image: string;
  contentId: string | null;
  version: string;
  items: IMarketplaceItem[];
  playlistTranslations: IMarketplacePlaylistTranslation[];
  categoryIds: number[];
}
