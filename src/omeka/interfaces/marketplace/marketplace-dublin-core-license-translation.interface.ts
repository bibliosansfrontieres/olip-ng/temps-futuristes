import { IMarketplaceLanguage } from './marketplace-language.interface';

export interface IMarketplaceDublinCoreLicenseTranslation {
  id: number;
  label: string;
  description: string;
  language: IMarketplaceLanguage;
}
