import { IFileInformations } from './file-informations.interface';
import { IItemInformations } from './item-informations.interface';

export interface IItem
  extends Omit<IItemInformations, 'file'>,
    IFileInformations {}
