import { IOmekaElementText } from './omeka-element-text.interface';

export interface IOmekaItem {
  id: number;
  url: string;
  public: boolean;
  featured: boolean;
  added: string;
  modified: string;
  item_type: {
    id: number;
    url: string;
    name: string;
    resource: string;
  };
  collection: null;
  owner: {
    id: number;
    url: string;
    resource: 'users';
  };
  files: {
    count: number;
    url: string;
    resource: 'files';
  };
  tags: { id: number; url: string; name: string; resource: 'tags' }[];
  element_texts: IOmekaElementText[];
  extended_resources: [];
}
