export interface IOmekaItemType {
  id: number;
  url: string;
  name: string;
  description: string;
  elements: { id: number; url: string }[];
  items: {
    count: number;
    url: string;
    resource: 'items';
  };
  extended_resources: [];
}
