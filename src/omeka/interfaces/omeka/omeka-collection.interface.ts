import { IOmekaElementText } from './omeka-element-text.interface';

export interface IOmekaCollection {
  id: number;
  url: string;
  public: boolean;
  featured: boolean;
  added: string;
  modified: string;
  owner: {
    id: number;
    url: string;
    resource: 'users';
  };
  items: {
    count: number;
    url: string;
    resource: 'items';
  };
  element_texts: IOmekaElementText[];
  extended_resources: [];
}
