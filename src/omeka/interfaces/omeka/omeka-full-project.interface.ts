import { IOmekaFile } from './omeka-file.interface';
import { IOmekaItem } from './omeka-item.interface';
import { IOmekaPackage } from './omeka-package.interface';

export interface IOmekaFullProject {
  omekaProject: IOmekaItem;
  omekaPackages: IOmekaPackage[];
  omekaItems: IOmekaItem[];
  omekaFiles: IOmekaFile[];
}
