export interface IOmekaFile {
  id: number;
  url: string;
  file_urls: {
    original: string;
    fullsize: string;
    thumbnail: string;
    square_thumbnail: string;
  };
  added: string;
  modified: string;
  filename: string;
  authentication: string;
  has_derivative_image: boolean;
  mime_type: string;
  order: null;
  original_filename: string;
  size: number;
  stored: boolean;
  type_os: string;
  metadata: {
    mime_type: string;
  };
  item: {
    id: number;
    url: string;
    resource: 'items';
  };
  element_texts: [];
  extended_resources: [];
}
