import { IPlaylistInformations } from './playlist-informations.interface';
import { IItem } from './item.interface';

export interface IPlaylist extends Omit<IPlaylistInformations, 'items'> {
  items: IItem[];
}
