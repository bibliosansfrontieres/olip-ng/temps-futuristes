import { IProjectInformations } from './project-informations.interface';

export interface IProject extends IProjectInformations {}
