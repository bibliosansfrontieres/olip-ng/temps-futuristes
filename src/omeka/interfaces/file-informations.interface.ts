export interface IFileInformations {
  file: string;
  thumbnail: string;
  mimeType: string;
  fileSize: number;
}
