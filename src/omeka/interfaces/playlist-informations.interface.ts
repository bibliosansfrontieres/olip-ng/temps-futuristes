export interface IPlaylistInformations {
  id: number;
  slug: string;
  description: string;
  goal: string;
  language: string;
  lastExportableModification: string;
  ideascubeName: string;
  subject: string;
  items: number[];
}
