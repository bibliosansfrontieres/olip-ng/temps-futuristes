import { Inject, Injectable, forwardRef } from '@nestjs/common';
import { IItem } from './interfaces/item.interface';
import { DublinCoreTypesService } from 'src/dublin-core-types/dublin-core-types.service';
import { IMarketplaceItem } from './interfaces/marketplace/marketplace-item.interface';
import { IMarketplacePlaylist } from './interfaces/marketplace/marketplace-playlist.interface';
import { IPlaylistInformations } from './interfaces/playlist-informations.interface';
import * as mime from 'mime-types';
import { CatalogService } from 'src/catalog/catalog.service';

@Injectable()
export class TranslateService {
  constructor(
    private dublinCoreTypesService: DublinCoreTypesService,
    @Inject(forwardRef(() => CatalogService))
    private catalogService: CatalogService,
  ) {}
  async translateProject(omekaProjectId: string) {
    const dublinCoreTypes = await this.dublinCoreTypesService.find();
    return { dublinCoreTypes };
  }

  async translateItem(item: IItem) {
    const dublinCoreType = await this.dublinCoreTypesService.findOne({
      where: { dublinCoreTypeTranslations: { label: item.itemType } },
    });
    const extension = mime.lookup(item.mimeType) || '';
    const marketplaceItem: IMarketplaceItem = {
      thumbnail: item.thumbnail,
      createdCountry: '',
      isInstalled: false,
      isUpdateNeeded: false,
      isBroken: false,
      isManuallyInstalled: false,
      version: null,
      contentId: null,
      dublinCoreItem: {
        dublinCoreLanguages: [],
        dublinCoreLicenses: [],
        dublinCoreAudiences: [],
        dublinCoreTypeId: dublinCoreType.id,
        dublinCoreItemTranslations: [
          {
            title: item.title,
            description: item.description,
            creator: item.creator,
            publisher: item.publisher,
            source: item.source,
            extent: '',
            subject: item.subject,
            dateCreated: item.dateCreated,
            languageIdentifier: 'eng',
          },
        ],
      },
      // TODO: File is not good (pages)
      file: {
        url: item.file,
        size: item.fileSize.toString(),
        mimeType: item.mimeType,
        duration: item.duration,
        pages: '0',
        extension,
      },
    };
    return marketplaceItem;
  }

  async translatePlaylist(
    playlist: IPlaylistInformations,
    items: IMarketplaceItem[],
  ) {
    const category = await this.catalogService.findOneCategory({
      where: {
        categoryTranslations: { title: playlist.subject.toLowerCase() },
      },
    });
    const marketplacePlaylist: IMarketplacePlaylist = {
      image: 'images/',
      contentId: playlist.id.toString(),
      version: playlist.lastExportableModification,
      items,
      playlistTranslations: [
        {
          title: playlist.ideascubeName,
          description: playlist.description,
          languageIdentifier: 'eng',
        },
      ],
      categoryIds: [category.id],
    };
    return marketplacePlaylist;
  }
}
