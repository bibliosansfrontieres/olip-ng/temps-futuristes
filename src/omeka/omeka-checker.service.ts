import { Inject, Injectable, forwardRef } from '@nestjs/common';
import { IOmekaElementText } from './interfaces/omeka/omeka-element-text.interface';
import { ItemErrorDto } from './dto/item-error.dto';
import { IOmekaItem } from './interfaces/omeka/omeka-item.interface';
import { PackageErrorDto } from './dto/package-error.dto';
import { IOmekaPackage } from './interfaces/omeka/omeka-package.interface';
import { ProjectErrorDto } from './dto/project-error.dto';
import { IOmekaFullProject } from './interfaces/omeka/omeka-full-project.interface';
import { ProjectErrorsDto } from './dto/project-errors.dto';
import { TransformerService } from 'src/transformer/transformer.service';

@Injectable()
export class OmekaCheckerService {
  constructor(private transformerService: TransformerService) {}

  async checkFullProject(
    fullProject: IOmekaFullProject,
  ): Promise<ProjectErrorsDto> {
    const olipProject =
      this.transformerService.transformProjectFromOmeka(fullProject);
    const projectErrors = await this.checkOmekaProject(
      fullProject.omekaProject,
    );
    const packagesErrors = await this.checkOmekaPackages(
      fullProject.omekaPackages,
    );
    const itemsErrors = await this.checkOmekaItems(fullProject.omekaItems);
    const count =
      projectErrors.length + packagesErrors.length + itemsErrors.length;
    return new ProjectErrorsDto(
      olipProject,
      count,
      projectErrors,
      packagesErrors,
      itemsErrors,
    );
  }

  checkOmekaProject(omekaProject: IOmekaItem): ProjectErrorDto[] {
    const projectErrors: ProjectErrorDto[] = [];
    const id = omekaProject.id;
    const elementTexts = omekaProject.element_texts;

    if (!this.findElement(50, elementTexts)) {
      projectErrors.push(new ProjectErrorDto(id, 'title not found'));
    }
    /*
    if (!this.findElement(40, elementTexts)) {
      projectErrors.push(new ProjectErrorDto(id, 'date not found'));
    }
    */
    if (!this.findElement(44, elementTexts)) {
      projectErrors.push(new ProjectErrorDto(id, 'languages not found'));
    }
    if (!this.findElement(259, elementTexts)) {
      projectErrors.push(new ProjectErrorDto(id, 'start date not found'));
    }
    if (!this.findElement(260, elementTexts)) {
      projectErrors.push(new ProjectErrorDto(id, 'partners not found'));
    }
    if (!this.findElement(261, elementTexts)) {
      projectErrors.push(new ProjectErrorDto(id, 'location not found'));
    }
    if (!this.findElement(262, elementTexts)) {
      projectErrors.push(new ProjectErrorDto(id, 'device not found'));
    }
    if (!this.findElement(263, elementTexts)) {
      projectErrors.push(new ProjectErrorDto(id, 'project manager not found'));
    }
    if (!this.findElement(264, elementTexts)) {
      projectErrors.push(new ProjectErrorDto(id, 'end date not found'));
    }

    return projectErrors;
  }

  async checkOmekaPackages(omekaPackages: IOmekaPackage[]) {
    const packagesErrors: PackageErrorDto[] = [];

    for (const omekaPackage of omekaPackages) {
      const id = omekaPackage.id;

      if (!omekaPackage.slug) {
        packagesErrors.push(new PackageErrorDto(id, 'slug not found'));
      }
      if (!omekaPackage.description) {
        packagesErrors.push(new PackageErrorDto(id, 'description not found'));
      }
      if (!omekaPackage.goal) {
        // TODO: COMMENTED FOR DEVELOPING PURPOSES (OMEKA PACKAGES ARE NOT ALL CORRECT...)
        //packagesErrors.push(new PackageErrorDto(id, 'goal not found'));
        omekaPackage.goal = 'Cultures';
      }
      if (!omekaPackage.language) {
        packagesErrors.push(new PackageErrorDto(id, 'language not found'));
      }
      if (!omekaPackage.last_exportable_modification) {
        packagesErrors.push(
          new PackageErrorDto(id, 'last_exportable_modification not found'),
        );
      }
      if (!omekaPackage.ideascube_name) {
        packagesErrors.push(
          new PackageErrorDto(id, 'ideascube_name not found'),
        );
      }
      if (!omekaPackage.subject) {
        // TODO: COMMENTED FOR DEVELOPING PURPOSES (OMEKA PACKAGES ARE NOT ALL CORRECT...)
        //packagesErrors.push(new PackageErrorDto(id, 'subject not found'));
        omekaPackage.subject = 'Cultures';
      }
    }

    return packagesErrors;
  }

  checkOmekaPackage(omekaPackage: IOmekaPackage) {
    const packagesErrors: PackageErrorDto[] = [];
    const id = omekaPackage.id;

    if (!omekaPackage.slug) {
      packagesErrors.push(new PackageErrorDto(id, 'slug not found'));
    }
    if (!omekaPackage.description) {
      packagesErrors.push(new PackageErrorDto(id, 'description not found'));
    }
    if (!omekaPackage.goal) {
      // TODO: COMMENTED FOR DEVELOPING PURPOSES (OMEKA PACKAGES ARE NOT ALL CORRECT...)
      //packagesErrors.push(new PackageErrorDto(id, 'goal not found'));
      omekaPackage.goal = 'Cultures';
    }
    if (!omekaPackage.language) {
      packagesErrors.push(new PackageErrorDto(id, 'language not found'));
    }
    if (!omekaPackage.last_exportable_modification) {
      packagesErrors.push(
        new PackageErrorDto(id, 'last_exportable_modification not found'),
      );
    }
    if (!omekaPackage.ideascube_name) {
      packagesErrors.push(new PackageErrorDto(id, 'ideascube_name not found'));
    }
    if (!omekaPackage.subject) {
      // TODO: COMMENTED FOR DEVELOPING PURPOSES (OMEKA PACKAGES ARE NOT ALL CORRECT...)
      //packagesErrors.push(new PackageErrorDto(id, 'subject not found'));
      omekaPackage.subject = 'Cultures';
    }

    return packagesErrors;
  }

  async checkOmekaItems(omekaItems: IOmekaItem[]) {
    const itemsErrors: ItemErrorDto[] = [];

    for (const omekaItem of omekaItems) {
      const id = omekaItem.id;

      if (omekaItem.url === undefined) {
        itemsErrors.push(new ItemErrorDto(id, 'url not found'));
      }
      if (omekaItem.public === undefined) {
        itemsErrors.push(new ItemErrorDto(id, 'public not found'));
      }
      if (omekaItem.featured === undefined) {
        itemsErrors.push(new ItemErrorDto(id, 'featured not found'));
      }
      if (omekaItem.added === undefined) {
        itemsErrors.push(new ItemErrorDto(id, 'added not found'));
      }
      if (omekaItem.modified === undefined) {
        itemsErrors.push(new ItemErrorDto(id, 'modified not found'));
      }
      /*
      if (
        omekaItem.item_type === undefined ||
        omekaItem.item_type?.id === undefined
      ) {
        itemsErrors.push(new ItemErrorDto(id, 'item_type not found'));
      }*/
      // TODO: collection ? We only use playlists collections in OLIP v2
      if (omekaItem.owner === undefined || omekaItem.owner.id === undefined) {
        itemsErrors.push(new ItemErrorDto(id, 'owner not found'));
      }
      if (omekaItem.files === undefined || omekaItem.files.count === 0) {
        itemsErrors.push(new ItemErrorDto(id, 'files not found'));
      }

      const elementTexts = omekaItem.element_texts;

      if (!this.findElement(50, elementTexts)) {
        itemsErrors.push(new ItemErrorDto(id, 'title not found'));
      }
      if (!this.findElement(41, elementTexts)) {
        itemsErrors.push(new ItemErrorDto(id, 'description not found'));
      }
      /*
      if (!this.findElement(39, elementTexts)) {
        itemsErrors.push(new ItemErrorDto(id, 'creator date not found'));
      }
      */
      // TODO : disabled publisher
      /*if (!this.findElement(45, elementTexts)) {
        itemsErrors.push(new ItemErrorDto(id, 'publisher not found'));
      }*/
      // TODO : disabled date created
      /*
      if (!this.findElement(56, elementTexts)) {
        itemsErrors.push(new ItemErrorDto(id, 'date created not found'));
      }*/
      if (!this.findElement(44, elementTexts)) {
        itemsErrors.push(new ItemErrorDto(id, 'language not found'));
      }
      // TODO : disabled source
      /*
      if (!this.findElement(48, elementTexts)) {
        itemsErrors.push(new ItemErrorDto(id, 'source not found'));
      }
      */
      // TODO : disabled duration
      /*
      if (!this.findElement(11, elementTexts)) {
        itemsErrors.push(new ItemErrorDto(id, 'duration not found'));
      }*/
      // TODO : disabled size
      /*if (!this.findElement(277, elementTexts)) {
        itemsErrors.push(new ItemErrorDto(id, 'size not found'));
      }*/
      /*
      if (!this.findElement(86, elementTexts)) {
        itemsErrors.push(new ItemErrorDto(id, 'audience not found'));
      }*/
      // TODO : disabled subject
      /*
      if (!this.findElement(49, elementTexts)) {
        itemsErrors.push(new ItemErrorDto(id, 'subject not found'));
      }
      */
      // TODO : disabled format
      /*
      if (!this.findElement(42, elementTexts)) {
        itemsErrors.push(new ItemErrorDto(id, 'format not found'));
      }*/
      // TODO : disabled type
      /*
      if (!this.findElement(51, elementTexts)) {
        itemsErrors.push(new ItemErrorDto(id, 'type not found'));
      }*/
    }

    return itemsErrors;
  }

  checkOmekaItem(omekaItem: IOmekaItem) {
    const itemsErrors: ItemErrorDto[] = [];

    const id = omekaItem.id;

    if (omekaItem.url === undefined) {
      itemsErrors.push(new ItemErrorDto(id, 'url not found'));
    }
    if (omekaItem.public === undefined) {
      itemsErrors.push(new ItemErrorDto(id, 'public not found'));
    }
    if (omekaItem.featured === undefined) {
      itemsErrors.push(new ItemErrorDto(id, 'featured not found'));
    }
    if (omekaItem.added === undefined) {
      itemsErrors.push(new ItemErrorDto(id, 'added not found'));
    }
    if (omekaItem.modified === undefined) {
      itemsErrors.push(new ItemErrorDto(id, 'modified not found'));
    }
    /*
    if (
      omekaItem.item_type === undefined ||
      omekaItem.item_type?.id === undefined
    ) {
      itemsErrors.push(new ItemErrorDto(id, 'item_type not found'));
    }*/
    // TODO: collection ? We only use playlists collections in OLIP v2
    if (omekaItem.owner === undefined || omekaItem.owner.id === undefined) {
      itemsErrors.push(new ItemErrorDto(id, 'owner not found'));
    }
    if (omekaItem.files === undefined || omekaItem.files.count === 0) {
      itemsErrors.push(new ItemErrorDto(id, 'files not found'));
    }

    const elementTexts = omekaItem.element_texts;

    if (!this.findElement(50, elementTexts)) {
      itemsErrors.push(new ItemErrorDto(id, 'title not found'));
    }
    if (!this.findElement(41, elementTexts)) {
      itemsErrors.push(new ItemErrorDto(id, 'description not found'));
    }
    if (!this.findElement(39, elementTexts)) {
      itemsErrors.push(new ItemErrorDto(id, 'creator date not found'));
    }
    if (!this.findElement(45, elementTexts)) {
      itemsErrors.push(new ItemErrorDto(id, 'publisher not found'));
    }
    if (!this.findElement(56, elementTexts)) {
      itemsErrors.push(new ItemErrorDto(id, 'date created not found'));
    }
    if (!this.findElement(44, elementTexts)) {
      itemsErrors.push(new ItemErrorDto(id, 'language not found'));
    }
    if (!this.findElement(48, elementTexts)) {
      itemsErrors.push(new ItemErrorDto(id, 'source not found'));
    }
    if (!this.findElement(11, elementTexts)) {
      itemsErrors.push(new ItemErrorDto(id, 'duration not found'));
    }
    if (!this.findElement(277, elementTexts)) {
      itemsErrors.push(new ItemErrorDto(id, 'size not found'));
    }
    if (!this.findElement(86, elementTexts)) {
      itemsErrors.push(new ItemErrorDto(id, 'audience not found'));
    }
    if (!this.findElement(49, elementTexts)) {
      itemsErrors.push(new ItemErrorDto(id, 'subject not found'));
    }
    if (!this.findElement(42, elementTexts)) {
      itemsErrors.push(new ItemErrorDto(id, 'format not found'));
    }
    if (!this.findElement(51, elementTexts)) {
      itemsErrors.push(new ItemErrorDto(id, 'type not found'));
    }

    return itemsErrors;
  }

  findElement(id: number, element_texts: IOmekaElementText[]) {
    const foundElement = element_texts.find(
      (element_text) => element_text.element.id === id,
    );
    return (
      foundElement !== undefined &&
      foundElement.text !== null &&
      foundElement.text !== undefined &&
      foundElement.text !== ''
    );
  }
}
