import { BadRequestException, Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { IOmekaItem } from './interfaces/omeka/omeka-item.interface';
import { IOmekaPackageRelation } from './interfaces/omeka/omeka-package-relation.interface';
import { IOmekaPackage } from './interfaces/omeka/omeka-package.interface';
import { IOmekaItemType } from './interfaces/omeka/omeka-item-type.interface';
import { IOmekaFile } from './interfaces/omeka/omeka-file.interface';
import { IOmekaCollection } from './interfaces/omeka/omeka-collection.interface';

@Injectable()
export class OmekaService {
  private omekaApiUrl: string;
  private omekaApiKey: string;
  private maxFetchingThreads: number;

  constructor(private configService: ConfigService) {
    process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0';
    this.maxFetchingThreads = 20;
    this.omekaApiUrl = this.configService.get('OMEKA_API_URL');
    this.omekaApiKey = this.configService.get('OMEKA_API_KEY');
  }

  async api(endpoint: string) {
    try {
      const query = endpoint.includes('?') ? '&' : '?';
      const result = await fetch(
        `${this.omekaApiUrl}/${endpoint}${query}key=${this.omekaApiKey}`,
        {},
      );
      return result.json();
    } catch (error) {
      console.error(error);
      return undefined;
    }
  }

  async getItems(page?: number, itemTypeId?: number) {
    if (page <= 0 || page === undefined) {
      page = 1;
    }
    const itemType = itemTypeId ? `&item_type=${itemTypeId}` : '';
    return (await this.api(`items?page=${page}${itemType}`)) as IOmekaItem[];
  }
  async getProjects(projectId?: number) {
    let omekaProjects: IOmekaItem[] = [];
    const omekaProjectsPackages: number[][] = [];
    let page = 1;
    let pageResults = [];
    do {
      pageResults = await this.getItems(page, 19);
      if (pageResults.length > 0) {
        omekaProjects.push(...pageResults);
      }
      page++;
    } while (pageResults.length !== 0);
    if (projectId) {
      omekaProjects = omekaProjects.filter(
        (project) => project.id === projectId,
      );
      if (omekaProjects.length === 0) {
        Logger.error(`Project ${projectId} not found in Omeka`);
      }
    }
    Logger.debug(`Made ${page - 1} calls to get projects`);
    for (const omekaProject of omekaProjects) {
      const omekaProjectPackageIds = await this.getProjectPackageIds(
        omekaProject.id,
      );
      omekaProjectsPackages.push(omekaProjectPackageIds);
    }
    return { omekaProjects, omekaProjectsPackages };
  }

  async getProject(id: number): Promise<IOmekaItem> {
    const omekaProject = (await this.api(`items/${id}`)) as IOmekaItem;

    if (omekaProject.id === undefined) {
      throw new BadRequestException(`project ID ${id} not found in Omeka`);
    }

    // Item Type 19 is a "Projet BSF"
    if (omekaProject.item_type.id !== 19) {
      throw new BadRequestException(`item ID ${id} is not a project`);
    }

    return omekaProject;
  }

  async getProjectPackageIds(omekaProjectId: number) {
    try {
      // Get packages in project from Omeka
      const omekaPackageRelations = (await this.api(
        `package_relations?item_id=${omekaProjectId}`,
      )) as IOmekaPackageRelation[];

      const omekaPackagesIds = omekaPackageRelations.map(
        (packageRelation) => packageRelation.package_id,
      );
      return omekaPackagesIds;
    } catch {
      return [];
    }
  }

  async getPackage(omekaPackageId: number) {
    return (await this.api(`packages/${omekaPackageId}`)) as IOmekaPackage;
  }

  async getPackages(
    projectPackageIds: number[],
  ): Promise<{ omekaPackages: IOmekaPackage[]; omekaItemIds: number[] }> {
    // Array to store fetch package promises
    const getOmekaPackages = [];

    for (const projectPackageId of projectPackageIds) {
      // Push all package fetch promises into getOmekaPackages array
      getOmekaPackages.push(this.getOmekaPackage(projectPackageId));
    }

    // Wait for all package fetches
    const omekaPackages = await Promise.all(getOmekaPackages);

    // Get item IDs from packages
    const omekaItemIds = [];
    for (const omekaPackage of omekaPackages) {
      for (const content of omekaPackage.contents) {
        // Only add item ID if not already in array
        if (!omekaItemIds.includes(content.item_id)) {
          omekaItemIds.push(content.item_id);
        }
      }
    }

    return {
      omekaPackages,
      omekaItemIds,
    };
  }

  async getItemsAndFiles(
    omekaProjectId: number,
    omekaItemIds: number[],
    omekaItems: IOmekaItem[] = [],
    omekaFiles: IOmekaFile[] = [],
  ) {
    const omekaItemIdsQueue = [...omekaItemIds];
    const threads = [];

    // Run X fetching threads to fetch files and items
    for (
      let threadIndex = 0;
      threadIndex < this.maxFetchingThreads &&
      threadIndex < omekaItemIds.length;
      threadIndex++
    ) {
      threads.push(
        this.getOmekaItemAndFileThread(
          omekaItemIdsQueue,
          omekaItems,
          omekaFiles,
        ),
      );
    }

    Logger.log(
      `[${omekaProjectId}] - Omeka Fetching 4/4 - Waiting for ${threads.length} threads to finish fetching items and files...`,
      'OmekaService',
    );
    await Promise.all(threads);

    // Check if any missing item in omekaItemIds
    const missingItemIds = omekaItemIds.filter(
      (itemId) => !omekaItems.some((item) => item.id === itemId),
    );

    if (missingItemIds.length > 0) {
      Logger.warn(
        `[${omekaProjectId}] - Omeka Fetching 4/4 - ${missingItemIds.length} missing items found, restarting fetching threads...`,
        'OmekaService',
      );
      return this.getItemsAndFiles(
        omekaProjectId,
        missingItemIds,
        omekaItems,
        omekaFiles,
      );
    } else {
      return { omekaFiles, omekaItems };
    }
  }

  async getOmekaItemAndFileThread(
    idsQueue: number[],
    omekaItems: IOmekaItem[],
    omekaFiles: IOmekaFile[],
  ) {
    while (idsQueue.length > 0) {
      const itemId = idsQueue.shift();
      const file = await this.getOmekaFile(itemId);
      const item = await this.getOmekaItem(itemId);
      if (file && file[0] && item) {
        omekaFiles.push(file[0]);
        omekaItems.push(item);
      }
    }
  }

  async getOmekaItemTypes() {
    return this.api('item_types') as Promise<IOmekaItemType[]>;
  }

  async getOmekaCollections() {
    return this.api('collections') as Promise<IOmekaCollection[]>;
  }

  async getOmekaFile(itemId: number) {
    const omekaFile = (await this.api(`files?item=${itemId}`)) as IOmekaFile[];
    if (omekaFile.length === undefined || omekaFile.length === 0) {
      throw new BadRequestException(
        `File for item ${itemId} not found in Omeka`,
      );
    }
    return omekaFile;
  }

  async getOmekaPackage(id: number) {
    const omekaPackage = (await this.api(`packages/${id}`)) as IOmekaPackage;
    if (omekaPackage.id === undefined) {
      throw new BadRequestException(`Package ${id} not found in Omeka`);
    }
    return omekaPackage;
  }

  async getOmekaItem(id: number) {
    const omekaItem = (await this.api(`items/${id}`)) as IOmekaItem;
    if (omekaItem.id === undefined) {
      throw new BadRequestException(`Item ${id} not found in Omeka`);
    }
    return omekaItem;
  }
}
