import { Injectable, Logger } from '@nestjs/common';
import { IOmekaItem } from './interfaces/omeka/omeka-item.interface';
import { IOmekaFile } from './interfaces/omeka/omeka-file.interface';
import { IOmekaFullProject } from './interfaces/omeka/omeka-full-project.interface';
import { OmekaService } from './omeka.service';

@Injectable()
export class OmekaProjectDefinitionsService {
  constructor(private omekaService: OmekaService) {}

  async getProject(id: number): Promise<IOmekaFullProject> {
    Logger.log(
      `[${id}] - Omeka Fetching 1/4 - Getting project...`,
      'OmekaService',
    );
    const omekaProject = await this.omekaService.getProject(id);

    Logger.log(
      `[${id}] - Omeka Fetching 2/4 - Getting project <-> packages relations...`,
      'OmekaService',
    );
    const projectPackageIds = await this.omekaService.getProjectPackageIds(id);

    Logger.log(
      `[${id}] - Omeka Fetching 3/4 - Getting packages....`,
      'OmekaService',
    );
    const { omekaPackages, omekaItemIds } =
      await this.omekaService.getPackages(projectPackageIds);

    Logger.log(
      `[${id}] - Omeka Fetching 4/4 - Getting items and files....`,
      'OmekaService',
    );
    const { omekaFiles, omekaItems } = await this.omekaService.getItemsAndFiles(
      id,
      omekaItemIds,
    );

    Logger.log(`[${id}] - Omeka Fetching 4/4 - Done !`, 'OmekaService');
    return {
      omekaProject,
      omekaPackages,
      omekaItems,
      omekaFiles,
    };
  }

  async getOmekaItemsAndFilesByThreads(omekaItemIds: number[]) {
    const maxThreads = 20;
    const omekaItemIdsQueue = [...omekaItemIds];
    const omekaItems = [];
    const omekaFiles = [];
    const threads = [];
    for (let threadIndex = 0; threadIndex < maxThreads; threadIndex++) {
      threads.push(
        this.getOmekaItemAndFileThread(
          threadIndex + 1,
          omekaItemIdsQueue,
          omekaItems,
          omekaFiles,
        ),
      );
    }
    Logger.log(
      `Waiting for ${maxThreads} to finish fetching...`,
      'OmekaService',
    );
    await Promise.all(threads);
    const missingItemIds = omekaItemIds.filter(
      (itemId) => !omekaItems.some((item) => item.id === itemId),
    );
    if (missingItemIds.length > 0) {
      Logger.log(
        `${missingItemIds.length} missing items found, restarting fetching threads...`,
        'OmekaService',
      );
      return this.getOmekaItemsAndFilesByThreads(missingItemIds);
    } else {
      return { omekaFiles, omekaItems };
    }
  }

  async getOmekaItemAndFileThread(
    thread: number,
    idsQueue: number[],
    omekaItems: IOmekaItem[],
    omekaFiles: IOmekaFile[],
  ) {
    while (idsQueue.length > 0) {
      const itemId = idsQueue.shift();
      const file = await this.omekaService.getOmekaFile(itemId);
      const item = await this.omekaService.getOmekaItem(itemId);
      if (file && file[0] && item) {
        omekaFiles.push(file[0]);
        omekaItems.push(item);
      }
    }
  }
}
