import { Controller, Get, Param } from '@nestjs/common';
import { OmekaService } from './omeka.service';
import { TranslateService } from './translate.service';

@Controller('omeka')
export class OmekaController {
  constructor(private translateService: TranslateService) {}

  @Get('project-definition/:id')
  getProjectDefinition(@Param('id') id: string) {
    return this.translateService.translateProject(id);
  }
}
