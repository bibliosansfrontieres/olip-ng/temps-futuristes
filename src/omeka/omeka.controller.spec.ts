import { Test, TestingModule } from '@nestjs/testing';
import { OmekaController } from './omeka.controller';
import { OmekaService } from './omeka.service';

describe('OmekaController', () => {
  let controller: OmekaController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [OmekaController],
      providers: [OmekaService],
    }).compile();

    controller = module.get<OmekaController>(OmekaController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
