import { IOlipProject } from 'src/transformer/interfaces/olip-project.interface';
import { ItemErrorDto } from '../dto/item-error.dto';
import { PackageErrorDto } from '../dto/package-error.dto';
import { ProjectErrorDto } from '../dto/project-error.dto';

export class ProjectErrorsDto {
  count: number;
  olipProject: IOlipProject;
  projectErrors: ProjectErrorDto[];
  packagesErrors: PackageErrorDto[];
  itemsErrors: ItemErrorDto[];
  constructor(
    olipProject: IOlipProject,
    count: number,
    projectErrors: ProjectErrorDto[],
    packagesErrors: PackageErrorDto[],
    itemsErrors: ItemErrorDto[],
  ) {
    this.olipProject = olipProject;
    this.count = count;
    this.projectErrors = projectErrors;
    this.packagesErrors = packagesErrors;
    this.itemsErrors = itemsErrors;
  }
}
