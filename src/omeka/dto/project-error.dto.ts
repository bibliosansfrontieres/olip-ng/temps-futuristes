import { ErrorDto } from './error.dto';

export class ProjectErrorDto extends ErrorDto {
  type = 'project';
}
