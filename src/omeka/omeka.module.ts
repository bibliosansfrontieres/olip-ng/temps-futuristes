import { Module, forwardRef } from '@nestjs/common';
import { OmekaService } from './omeka.service';
import { OmekaController } from './omeka.controller';
import { TranslateService } from './translate.service';
import { DublinCoreTypesModule } from 'src/dublin-core-types/dublin-core-types.module';
import { OmekaCheckerService } from './omeka-checker.service';
import { OmekaProjectDefinitionsService } from './omeka-project-definitions.service';
import { TransformerModule } from 'src/transformer/transformer.module';
import { OmekaCatalogService } from './omeka-catalog.service';
import { CatalogModule } from 'src/catalog/catalog.module';

@Module({
  imports: [
    DublinCoreTypesModule,
    TransformerModule,
    forwardRef(() => CatalogModule),
  ],
  controllers: [OmekaController],
  providers: [
    OmekaService,
    TranslateService,
    OmekaCheckerService,
    OmekaProjectDefinitionsService,
    OmekaCatalogService,
  ],
  exports: [
    OmekaService,
    OmekaCheckerService,
    OmekaProjectDefinitionsService,
    OmekaCatalogService,
  ],
})
export class OmekaModule {}
