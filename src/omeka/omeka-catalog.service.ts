import { Injectable, Logger } from '@nestjs/common';
import { OmekaService } from 'src/omeka/omeka.service';
import { IOmekaItem } from './interfaces/omeka/omeka-item.interface';
import { IOmekaPackage } from './interfaces/omeka/omeka-package.interface';
import { IOmekaFile } from './interfaces/omeka/omeka-file.interface';
import { IOmekaCollection } from './interfaces/omeka/omeka-collection.interface';

@Injectable()
export class OmekaCatalogService {
  constructor(private omekaService: OmekaService) {}

  async getAllItemsAndFiles(omekaItemIds: number[]) {
    const omekaItemsAndFiles: {
      omekaItem: IOmekaItem;
      omekaFile: IOmekaFile;
    }[] = [];
    const threads = [];
    for (let i = 0; i < 10 && i < omekaItemIds.length; i++) {
      threads.push(
        this.getItemsAndFilesThread(omekaItemIds, omekaItemsAndFiles),
      );
    }
    await Promise.all(threads);
    return omekaItemsAndFiles;
  }

  async getItemsAndFilesThread(
    omekaItemIds: number[],
    omekaItemsAndFiles: { omekaItem: IOmekaItem; omekaFile: IOmekaFile }[],
  ) {
    while (omekaItemIds.length > 0) {
      const omekaItemId = omekaItemIds.shift();
      try {
        console.log(omekaItemId, omekaItemIds.length);
        const omekaItem = await this.omekaService.getOmekaItem(omekaItemId);
        const omekaFile = (
          await this.omekaService.getOmekaFile(omekaItemId)
        )[0];
        omekaItemsAndFiles.push({ omekaItem, omekaFile });
      } catch (error) {
        console.error(error);
        Logger.error(`Error with file ${omekaItemId}`);
      }
    }
  }

  async getAllPackages(omekaPackageIds: number[]) {
    const omekaPackages: IOmekaPackage[] = [];
    const omekaPackagesItems: { packageId: number; itemIds: number[] }[] = [];
    const threads = [];
    for (let i = 0; i < 10 && i < omekaPackageIds.length; i++) {
      threads.push(
        this.getPackagesThread(
          omekaPackageIds,
          omekaPackages,
          omekaPackagesItems,
        ),
      );
    }
    await Promise.all(threads);
    const omekaItemIds = [];
    for (const omekaPackage of omekaPackages) {
      for (const content of omekaPackage.contents) {
        if (!omekaItemIds.includes(content.item_id)) {
          omekaItemIds.push(content.item_id);
        }
      }
    }
    return { omekaPackages, omekaPackagesItems, omekaItemIds };
  }

  async getPackagesThread(
    omekaPackageIds: number[],
    omekaPackages: IOmekaPackage[],
    omekaPackagesItems: { packageId: number; itemIds: number[] }[],
  ) {
    while (omekaPackageIds.length > 0) {
      const omekaPackageId = omekaPackageIds.shift();
      const omekaPackage = await this.omekaService.getPackage(omekaPackageId);
      const omekaPackageItems = omekaPackage.contents.map(
        (content) => content.item_id,
      );
      omekaPackages.push(omekaPackage);
      omekaPackagesItems.push({
        packageId: omekaPackageId,
        itemIds: omekaPackageItems,
      });
    }
  }

  async getAllPackageIds(omekaProjects: IOmekaItem[]) {
    const omekaPackageIds = [];
    const threads = [];
    for (let i = 0; i < 10 && i < omekaProjects.length; i++) {
      threads.push(this.getPackageIdsThread(omekaProjects, omekaPackageIds));
    }
    await Promise.all(threads);
    return [...new Set(omekaPackageIds)];
  }

  async getPackageIdsThread(
    omekaProjects: IOmekaItem[],
    omekaPackageIds: number[],
  ) {
    while (omekaProjects.length > 0) {
      const omekaProject = omekaProjects.shift();
      const omekaProjectPackageIds =
        await this.omekaService.getProjectPackageIds(omekaProject.id);
      omekaPackageIds.push(...omekaProjectPackageIds);
    }
  }

  async getOmekaCatalog(projectId?: number) {
    const { omekaProjects, omekaProjectsPackages } =
      await this.omekaService.getProjects(projectId);

    const omekaPackageIds: number[] = [];
    for (const omekaProjectPackages of omekaProjectsPackages) {
      for (const omekaProjectPackage of omekaProjectPackages) {
        if (!omekaPackageIds.includes(omekaProjectPackage)) {
          omekaPackageIds.push(omekaProjectPackage);
        }
      }
    }

    const omekaPackagesCalls = omekaPackageIds.length;
    const { omekaPackages, omekaPackagesItems, omekaItemIds } =
      await this.getAllPackages(omekaPackageIds);
    Logger.debug(
      `Made ${omekaPackagesCalls} calls to get packages and item IDs`,
    );
    const omekaItemsAndFilesCalls = omekaItemIds.length;
    const omekaItemsAndFiles = await this.getAllItemsAndFiles(omekaItemIds);
    Logger.debug(
      `Made ${omekaItemsAndFilesCalls} calls to get items and files`,
    );

    const omekaCollections = await this.getOmekaCollections();

    return {
      omekaProjects,
      omekaProjectsPackages,
      omekaPackagesItems,
      omekaPackages,
      omekaItemsAndFiles,
      omekaCollections,
    };
  }

  async getOmekaCollections(): Promise<IOmekaCollection[]> {
    return this.omekaService.getOmekaCollections();
  }

  async getOmekaProject(projectId: string) {
    const omekaProject = await this.omekaService.getProject(+projectId);

    const { omekaPackages, omekaItemIds } = await this.omekaService.getPackages(
      [+projectId],
    );

    const omekaItemsAndFiles = [];

    await this.getItemsAndFilesThread(omekaItemIds, omekaItemsAndFiles);

    return {
      omekaProject,
      omekaPackages,
      omekaItemsAndFiles,
    };
  }
}
