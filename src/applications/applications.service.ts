import { Injectable, NotFoundException } from '@nestjs/common';
import { readFileSync, readdirSync } from 'fs';
import { join } from 'path';
import { IApplication } from './interfaces/application.interface';
import { IApplicationDescription } from './interfaces/application-description.interface';
import { IApplicationHook } from './interfaces/application-hook.interface';
import { XMLParser } from 'fast-xml-parser';

@Injectable()
export class ApplicationsService {
  getApplications() {
    const applications: IApplication[] = [];
    const applicationsPath = join(__dirname, '../../data/applications');
    const applicationsDirs = readdirSync(applicationsPath);
    for (const applicationDir of applicationsDirs) {
      const applicationPath = join(applicationsPath, applicationDir);

      const descriptionPath = join(applicationPath, 'description.json');
      const descriptionContent = readFileSync(descriptionPath).toString();
      const description = JSON.parse(
        descriptionContent,
      ) as IApplicationDescription;

      const hooksPath = join(applicationPath, 'hooks.json');
      const hooksContent = readFileSync(hooksPath).toString();
      const hooks = JSON.parse(hooksContent) as { hooks: IApplicationHook[] };

      const composePath = join(applicationPath, 'docker-compose.yml');
      const composeContent = readFileSync(composePath).toString();
      const compose = Buffer.from(composeContent).toString('base64');

      const application: IApplication = {
        name: applicationDir,
        description,
        hooks: hooks.hooks,
        compose,
      };

      applications.push(application);
    }
    return applications;
  }

  async getApplication(name: string) {
    const applications = this.getApplications();
    const application = applications.find((app) => app.name === name);
    if (!application) throw new NotFoundException('application not found');
    return application;
  }

  async getApplicationCatalog(name: string) {
    const application = await this.getApplication(name);
    if (!application) throw new NotFoundException('application not found');
    const { transformCatalog } = await import(
      join(__dirname, `../../data/applications/${name}/get-catalog.js`)
    );
    if (application.description.catalog) {
      const fetchCatalog = await fetch(application.description.catalog.url);
      const catalog = await fetchCatalog.text();
      const parser = new XMLParser({ ignoreAttributes: false });
      const jsonObject = parser.parse(catalog);
      return transformCatalog(jsonObject);
    }
    return [];
  }

  async getApplicationCatalogContent(name: string, title: string) {
    const catalog = await this.getApplicationCatalog(name);
    const content = catalog.find((c) => c.title === title);
    if (!content) throw new NotFoundException('content not found');
    return content;
  }

  async getApplicationTypes() {
    return [
      {
        name: 'content_host',
        typeTranslations: [
          {
            languageIdentifier: 'eng',
            label: 'Content Host',
            description: 'Application used to host contents',
          },
          {
            languageIdentifier: 'fra',
            label: 'Hébergeur de contenus',
            description: 'Application utilisée pour héberger des contenus',
          },
        ],
      },
      {
        name: 'storage',
        typeTranslations: [
          {
            languageIdentifier: 'eng',
            label: 'Storage',
            description: 'Application used to store files',
          },
          {
            languageIdentifier: 'fra',
            label: 'Stockage',
            description: 'Application utilisée pour stocker des fichiers',
          },
        ],
      },
    ];
  }
}
