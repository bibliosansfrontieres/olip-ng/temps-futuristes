import { Controller, Get, Param } from '@nestjs/common';
import { ApplicationsService } from './applications.service';

@Controller('applications')
export class ApplicationsController {
  constructor(private readonly applicationsService: ApplicationsService) {}

  @Get()
  getApplications() {
    return this.applicationsService.getApplications();
  }

  @Get('types')
  getApplicationTypes() {
    return this.applicationsService.getApplicationTypes();
  }

  @Get('catalog/:name')
  getApplicationCatalog(@Param('name') name: string) {
    return this.applicationsService.getApplicationCatalog(name);
  }

  @Get(':name')
  getApplication(@Param('name') name: string) {
    return this.applicationsService.getApplication(name);
  }

  @Get('catalog/:name/:title')
  getApplicationContent(
    @Param('name') name: string,
    @Param('title') title: string,
  ) {
    return this.applicationsService.getApplicationCatalogContent(name, title);
  }
}
