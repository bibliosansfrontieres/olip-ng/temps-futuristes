import { IApplicationDescription } from './application-description.interface';
import { IApplicationHook } from './application-hook.interface';

export interface IApplication {
  name: string;
  compose: string;
  description: IApplicationDescription;
  hooks: IApplicationHook[];
}
