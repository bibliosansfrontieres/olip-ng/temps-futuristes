export interface IApplicationHook {
  name: string;
  exec?: string;
  value?: string;
}
