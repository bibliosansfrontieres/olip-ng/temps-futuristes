export interface IApplicationDescription {
  id: string;
  name: string;
  version: string;
  catalog: {
    type: 'xml';
    url: string;
  };
}
