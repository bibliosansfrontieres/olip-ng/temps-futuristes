import { Injectable, NestMiddleware } from '@nestjs/common';
import { createReadStream, existsSync } from 'fs';
import { resolve } from 'path';
import * as mime from 'mime-types';
import { Request, Response } from 'express';

const allowedExt = ['.ico', '.png', '.jpg', '.svg', '.webp'];

const resolvePath = (file: string) => resolve(`./data${file}`);

@Injectable()
export class ImagesMiddleware implements NestMiddleware {
  use(req: Request, res: Response) {
    const { originalUrl } = req;
    console.log(originalUrl);
    console.log(existsSync(resolvePath(originalUrl)));
    if (
      allowedExt.filter((ext) => originalUrl.indexOf(ext) > 0).length > 0 &&
      existsSync(resolvePath(originalUrl))
    ) {
      // if extension is allowed and file found, return the file
      const mimeType = mime.lookup(originalUrl) || 'text/plain';
      const stream = createReadStream(resolvePath(originalUrl));
      res.setHeader('Content-Type', mimeType);
      res.setHeader('Cache-Control', 'max-age=3600');
      return stream.pipe(res);
    } else {
      res.send('Not Found');
    }
  }
}
