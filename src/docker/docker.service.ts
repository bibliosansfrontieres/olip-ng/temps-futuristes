import { Injectable, Logger } from '@nestjs/common';
import { Instance } from 'src/instances/entities/instance.entity';
import { dockerCommand } from 'docker-cli-js';
import { join } from 'path';
import AnsiToHtml from 'ansi-to-html';
import { spawn } from 'child_process';
import { Response } from 'express';

@Injectable()
export class DockerService {
  async runInstance(instance: Instance) {
    const dockerComposePath = join(
      __dirname,
      `../../data/instances/${instance.vmId}/docker-compose.yml`,
    );
    const commandResult = await dockerCommand(
      `compose -f ${dockerComposePath} up -d --force-recreate`,
      {
        echo: false,
      },
    );
    Logger.log(`Instance ${instance.vmId} started !`, 'InstancesService');
    return commandResult;
  }

  async stopInstance(instance: Instance) {
    const dockerComposePath = join(
      __dirname,
      `../../data/instances/${instance.vmId}/docker-compose.yml`,
    );
    const commandResult = await dockerCommand(
      `compose -f ${dockerComposePath} down`,
      {
        echo: true,
      },
    );
    Logger.log(`Instance ${instance.vmId} stopped !`, 'InstancesService');
    return commandResult;
  }

  async getLogs(vmId: string) {
    try {
      const commandResult = await dockerCommand(`logs olip-${vmId} -n 200`, {
        echo: false,
      });
      const ansi = commandResult.raw;
      const ansiToHtml = new AnsiToHtml();
      const htmlOutput = ansiToHtml.toHtml(ansi);
      const htmlOutputWithBreaks = htmlOutput.replace(/\n/g, '<br>');
      const lines = htmlOutputWithBreaks.split('<br>');
      const linesToReturn = [];
      for (const line of lines) {
        if (!line.includes('Creating language')) {
          linesToReturn.push(line);
        }
      }
      return { html: linesToReturn.join('<br>') };
    } catch (e) {
      console.error(e);
      return { html: '' };
    }
  }

  /*
  async getLogsStream(vmId: string) {
    try {
      const commandResult = await dockerCommand(`logs olip-${vmId} -n 200 -f`, {
        echo: false,
      });
      console.log(commandResult);
      console.log('finished !');
    } catch (e) {
      console.error(e);
    }
  }
  */

  getLogsStream(response: Response, vmId: string): void {
    // Spawn the docker logs -f command as a child process
    const dockerLogsProcess = spawn('docker', [
      'logs',
      '-f',
      '-n 20',
      `olip-${vmId}`,
    ]);

    // Set up event listeners to handle data and close events
    dockerLogsProcess.stdout.on('data', (data) => {
      // Send the data to the client as it arrives
      response.write(data);
      console.log(data.toString());
    });

    dockerLogsProcess.stderr.on('data', (data) => {
      // Handle any errors
      console.error(`Error from docker logs process: ${data}`);
    });

    dockerLogsProcess.on('close', (code) => {
      // Close the response stream when the child process exits
      response.end();
      console.log(`docker logs process exited with code ${code}`);
    });

    // Handle the request being closed by the client
    response.on('close', () => {
      // Kill the docker logs process when the client disconnects
      dockerLogsProcess.kill();
    });
  }
}
