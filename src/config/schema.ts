import * as Joi from 'joi';

export const configSchema = Joi.object({
  MAESTRO_URL: Joi.string().required(),
  MAESTRO_URL_FOR_OLIP: Joi.string().required(),
  PORT: Joi.number().required(),
  OMEKA_API_KEY: Joi.string().required(),
  OMEKA_API_URL: Joi.string().required(),
  AZURE_AD_AUDIENCE: Joi.string().required(),
  AZURE_AD_CLIENT_ID: Joi.string().required(),
  AZURE_AD_TENANT_ID: Joi.string().required(),
  SHARED_FOLDER: Joi.string().required(),
  OLIP_NETWORK: Joi.string().required(),
  TLS_ENABLED: Joi.boolean().required(),
});
