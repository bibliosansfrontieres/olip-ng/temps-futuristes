import { MiddlewareConsumer, Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigModule } from '@nestjs/config';
import { configSchema } from './config/schema';
import { OmekaModule } from './omeka/omeka.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { InitDatabaseModule } from './init-database/init-database.module';
import { DublinCoreTypesModule } from './dublin-core-types/dublin-core-types.module';
import { DublinCoreType } from './dublin-core-types/entities/dublin-core-type.entity';
import { DublinCoreTypeTranslation } from './dublin-core-types/entities/dublin-core-type-translation.entity';
import { ProjectDefinitionsModule } from './project-definitions/project-definitions.module';
import { TransformerModule } from './transformer/transformer.module';
import { DockerModule } from './docker/docker.module';
import { InstancesModule } from './instances/instances.module';
import { Instance } from './instances/entities/instance.entity';
import { ApplicationsModule } from './applications/applications.module';
import { InstanceError } from './instances/entities/instance-error.entity';
import { InstanceProject } from './instances/entities/instance-project.entity';
import { CatalogModule } from './catalog/catalog.module';
import { Project } from './catalog/entities/project.entity';
import { Playlist } from './catalog/entities/playlist.entity';
import { PlaylistTranslation } from './catalog/entities/playlist-translation.entity';
import { Item } from './catalog/entities/item.entity';
import { ItemTranslation } from './catalog/entities/item-translation.entity';
import { UploadModule } from './upload/upload.module';
import { ImagesMiddleware } from './images.middleware';
import { Category } from './catalog/entities/category.entity';
import { CategoryTranslation } from './catalog/entities/category-translation.entity';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      validationSchema: configSchema,
    }),
    OmekaModule,
    TypeOrmModule.forRoot({
      type: 'sqlite',
      database: 'data/databases/maestro.db',
      entities: [
        DublinCoreType,
        DublinCoreTypeTranslation,
        Instance,
        InstanceError,
        InstanceProject,
        Project,
        Category,
        CategoryTranslation,
        Playlist,
        PlaylistTranslation,
        Item,
        ItemTranslation,
      ],
      synchronize: true,
    }),
    InitDatabaseModule,
    DublinCoreTypesModule,
    ProjectDefinitionsModule,
    TransformerModule,
    DockerModule,
    InstancesModule,
    ApplicationsModule,
    CatalogModule,
    UploadModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(ImagesMiddleware).forRoutes('/images/*');
  }
}
