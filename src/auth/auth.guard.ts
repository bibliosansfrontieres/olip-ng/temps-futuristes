import {
  CanActivate,
  ExecutionContext,
  Injectable,
  Logger,
  UnauthorizedException,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Request } from 'express';
import validate from 'validate-azure-ad-token';

/**
 * Extracts a Bearer token from the header of a request.
 *
 * @param {Request} request - The request object.
 * @return {string | undefined} The extracted token, or undefined if not found.
 */
export function extractTokenFromHeaders(request: Request): string | undefined {
  const [type, token] = request.headers.authorization?.split(' ') ?? [];
  return type === 'Bearer' ? token : undefined;
}

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private configService: ConfigService) {}

  /**
   * Asynchronously checks if the user can activate the current route.
   *
   * @param {ExecutionContext} context - the execution context
   * @return {Promise<boolean>} - a promise that resolves to a boolean indicating if the user can activate the route
   */
  async canActivate(context: ExecutionContext): Promise<boolean> {
    const request = context.switchToHttp().getRequest();
    const token = extractTokenFromHeaders(request);

    if (!token) {
      throw new UnauthorizedException();
    }

    try {
      await validate(token, {
        tenantId: this.configService.get('AZURE_AD_TENANT_ID'),
        audience: this.configService.get('AZURE_AD_AUDIENCE'),
        applicationId: this.configService.get('AZURE_AD_CLIENT_ID'),
        scopes: ['User.Read'],
      });
    } catch (error) {
      Logger.error('Error while decoding access token', 'AuthGuard');
      console.error(error);
      throw new UnauthorizedException();
    }
    return true;
  }
}
