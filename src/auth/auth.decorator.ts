import { UseGuards, applyDecorators } from '@nestjs/common';
import { AuthGuard } from './auth.guard';

/**
 * Creates an authentication decorator.
 * This decorator will check if user is connected by Bearer token.
 *
 */

export function Auth() {
  return applyDecorators(UseGuards(AuthGuard));
}
