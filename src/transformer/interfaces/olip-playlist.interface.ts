import { IOlipPlaylistTranslation } from './olip-playlist-translation.interface';

export interface IOlipPlaylist {
  id: string;
  image: string;
  updatedAt: string;
  playlistTranslations: IOlipPlaylistTranslation[];
  categoryIds: number[];
  itemIds: number[];
}
