import { DublinCoreType } from 'src/dublin-core-types/entities/dublin-core-type.entity';
import { OlipItemDto } from '../dto/olip-item.dto';
import { OlipPlaylistDto } from '../dto/olip-playlist.dto';
import { OlipProjectDto } from '../dto/olip-project.dto';
import { Category } from 'src/catalog/entities/category.entity';

export interface IOlipProjectDefinition {
  olipProject: OlipProjectDto;
  olipPlaylists: OlipPlaylistDto[];
  olipItems: OlipItemDto[];
  dublinCoreTypes: DublinCoreType[];
  categories: Category[];
}
