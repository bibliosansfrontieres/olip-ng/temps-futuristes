export interface IOlipPlaylistTranslation {
  title: string;
  description: string;
  languageIdentifier: string;
}
