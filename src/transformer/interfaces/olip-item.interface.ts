import { IOlipDublinCoreItem } from './olip-dublin-core-item.interface';
import { IOlipItemDocumentType } from './olip-item-document-type.interface';
import { IOlipItemLanguageLevel } from './olip-item-language-level.interface';

export interface IOlipItem {
  id: string;
  thumbnail: string;
  createdCountry: string;
  createdAt: string;
  updatedAt: string;
  contentId: string;
  url: string;
  dublinCoreItem: IOlipDublinCoreItem;
  itemDocumentType: IOlipItemDocumentType;
  itemLanguageLevel: IOlipItemLanguageLevel;
}
