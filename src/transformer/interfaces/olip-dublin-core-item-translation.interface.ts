export interface IOlipDublinCoreItemTranslation {
  title: string;
  description: string;
  creator: string;
  publisher: string;
  source: string;
  extent: string;
  subject: string;
  dateCreated: string;
  languageIdentifier: string;
}
