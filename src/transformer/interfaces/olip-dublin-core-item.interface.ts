import { IOlipDublinCoreItemTranslation } from './olip-dublin-core-item-translation.interface';

export interface IOlipDublinCoreItem {
  dublinCoreTypeId: number;
  dublinCoreItemTranslations: IOlipDublinCoreItemTranslation[];
}
