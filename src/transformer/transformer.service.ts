import { Injectable } from '@nestjs/common';
import { OmekaTransformerService } from './omeka-transformer.service';
import { IOlipProjectDefinition } from './interfaces/olip-project-definition.interface';
import { IOmekaFullProject } from 'src/omeka/interfaces/omeka/omeka-full-project.interface';
import { join } from 'path';
import { writeFileSync } from 'fs';

@Injectable()
export class TransformerService {
  constructor(private omekaTransformerService: OmekaTransformerService) {}

  async transformFromOmeka(
    fullProject: IOmekaFullProject,
  ): Promise<IOlipProjectDefinition> {
    const projectDefinition =
      await this.omekaTransformerService.transform(fullProject);

    for (const playlist of projectDefinition.olipPlaylists) {
      let size = 0;
      for (const itemId of playlist.itemIds) {
        const item = projectDefinition.olipItems.find((i) => +i.id === itemId);
        if (!item) continue;
        size += item.size;
      }
      playlist.size = size;
    }

    const projectDefinitionPath = join(
      __dirname,
      '../../data/project-definitions',
      `${projectDefinition.olipProject.id}.json`,
    );
    writeFileSync(projectDefinitionPath, JSON.stringify(projectDefinition));
    return projectDefinition;
  }

  transformProjectFromOmeka(fullProject: IOmekaFullProject) {
    return this.omekaTransformerService.transformProject(
      fullProject.omekaProject,
    );
  }
}
