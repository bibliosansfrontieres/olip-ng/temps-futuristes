import { Inject, Injectable, forwardRef } from '@nestjs/common';
import { IOlipProjectDefinition } from './interfaces/olip-project-definition.interface';
import { OlipProjectDto } from './dto/olip-project.dto';
import { OlipPlaylistDto } from './dto/olip-playlist.dto';
import { OlipItemDto } from './dto/olip-item.dto';
import { DublinCoreTypesService } from 'src/dublin-core-types/dublin-core-types.service';
import { IOmekaFullProject } from 'src/omeka/interfaces/omeka/omeka-full-project.interface';
import { IOmekaItem } from 'src/omeka/interfaces/omeka/omeka-item.interface';
import { IOmekaPackage } from 'src/omeka/interfaces/omeka/omeka-package.interface';
import { IOmekaFile } from 'src/omeka/interfaces/omeka/omeka-file.interface';
import { CatalogService } from 'src/catalog/catalog.service';
import { IOmekaCollection } from 'src/omeka/interfaces/omeka/omeka-collection.interface';
import { OlipCollection } from './dto/olip-collection.dto';

@Injectable()
export class OmekaTransformerService {
  constructor(
    private dublinCoreTypesService: DublinCoreTypesService,
    @Inject(forwardRef(() => CatalogService))
    private catalogService: CatalogService,
  ) {}

  transformProject(omekaProject: IOmekaItem): OlipProjectDto {
    const olipProject = new OlipProjectDto(omekaProject);
    return olipProject;
  }

  async transformPlaylist(omekaPackage: IOmekaPackage) {
    const category = await this.catalogService.findOneCategory({
      where: {
        categoryTranslations: { title: omekaPackage.subject.toLowerCase() },
      },
    });
    return new OlipPlaylistDto(omekaPackage, [category.id]);
  }

  transformCollection(omekaCollection: IOmekaCollection) {
    return new OlipCollection(omekaCollection);
  }

  transformItem(omekaItemAndFile: {
    omekaItem: IOmekaItem;
    omekaFile: IOmekaFile;
  }) {
    const { omekaItem, omekaFile } = omekaItemAndFile;
    if (omekaItem.item_type === null || omekaItem.item_type === undefined) {
      omekaItem.item_type = {
        id: 0,
        name: 'Unknown',
        url: '',
        resource: '',
      };
    }
    if (omekaFile.mime_type.includes('image/')) {
      omekaItem.item_type.id = 5;
    } else if (omekaFile.mime_type.includes('video/')) {
      omekaItem.item_type.id = 2;
    } else if (omekaFile.mime_type.includes('audio/')) {
      omekaItem.item_type.id = 4;
    } else if (omekaFile.mime_type.includes('application/pdf')) {
      omekaItem.item_type.id = 1;
    }
    return new OlipItemDto(omekaItem, omekaFile);
  }

  async transform(
    fullProject: IOmekaFullProject,
  ): Promise<IOlipProjectDefinition> {
    const olipProject = new OlipProjectDto(fullProject.omekaProject);
    const olipPlaylists = [];
    for (const omekaPackage of fullProject.omekaPackages) {
      const category = await this.catalogService.findOneCategory({
        where: {
          categoryTranslations: { title: omekaPackage.subject.toLowerCase() },
        },
      });
      if (category) {
        olipPlaylists.push(new OlipPlaylistDto(omekaPackage, [category.id]));
      } else {
        console.log(omekaPackage.id, omekaPackage.subject);
      }
    }
    const olipItems = [];
    for (const omekaItem of fullProject.omekaItems) {
      const omekaFile = fullProject.omekaFiles.find(
        (oF) => oF.item.id === omekaItem.id,
      );
      if (!omekaItem.item_type) {
        omekaItem.item_type = {} as any;
      }
      if (omekaFile.mime_type.includes('image/')) {
        omekaItem.item_type.id = 5;
      } else if (omekaFile.mime_type.includes('video/')) {
        omekaItem.item_type.id = 2;
      } else if (omekaFile.mime_type.includes('audio/')) {
        omekaItem.item_type.id = 4;
      } else if (omekaFile.mime_type.includes('application/pdf')) {
        omekaItem.item_type.id = 1;
      }
      olipItems.push(new OlipItemDto(omekaItem, omekaFile));
    }
    const categories = await this.catalogService.findAllCategories();
    const dublinCoreTypes = await this.dublinCoreTypesService.find();
    return {
      olipProject,
      olipPlaylists,
      olipItems,
      categories,
      dublinCoreTypes,
    };
  }
}
