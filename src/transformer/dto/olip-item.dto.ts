import { IOmekaItem } from 'src/omeka/interfaces/omeka/omeka-item.interface';
import { IOlipItem } from '../interfaces/olip-item.interface';
import { IOlipDublinCoreItem } from '../interfaces/olip-dublin-core-item.interface';
import { IOlipItemDocumentType } from '../interfaces/olip-item-document-type.interface';
import { IOlipItemLanguageLevel } from '../interfaces/olip-item-language-level.interface';
import { IOmekaFile } from 'src/omeka/interfaces/omeka/omeka-file.interface';
import { IOlipFile } from '../interfaces/olip-file.interface';
import { findElement } from 'src/utils/find-element.util';

export class OlipItemDto implements IOlipItem {
  id: string;
  thumbnail: string | null;
  createdCountry: string;
  createdAt: string;
  updatedAt: string;
  url: string | null;
  fileName: string;
  imageName: string;
  size: number;
  contentId: string;
  mimeType: string;
  dublinCoreItem: IOlipDublinCoreItem;
  itemDocumentType: IOlipItemDocumentType;
  itemLanguageLevel: IOlipItemLanguageLevel;
  file: IOlipFile;

  constructor(omekaItem: IOmekaItem, omekaFile: IOmekaFile) {
    this.id = omekaItem.id.toString();
    this.createdAt = omekaItem.added;
    this.updatedAt = omekaItem.modified;
    this.contentId = omekaItem.id.toString();
    this.thumbnail = omekaFile.file_urls?.thumbnail || null;
    // TODO: Created country is empty
    this.createdCountry = 'fr';
    this.url = omekaFile.file_urls?.original || null;
    const fileNameSplit = omekaFile.file_urls?.original?.split('/');
    this.fileName = fileNameSplit?.[fileNameSplit.length - 1];
    const imageNameSplit = omekaFile.file_urls?.thumbnail?.split('/');
    this.imageName = imageNameSplit?.[imageNameSplit.length - 1] || '';
    this.size = omekaFile.size;
    this.mimeType = omekaFile.mime_type;
    this.dublinCoreItem = {
      dublinCoreTypeId: omekaItem.item_type.id,
      dublinCoreItemTranslations: [
        {
          title: findElement(50, omekaItem.element_texts),
          description: findElement(41, omekaItem.element_texts),
          creator: findElement(39, omekaItem.element_texts),
          publisher: findElement(45, omekaItem.element_texts) || '',
          dateCreated: findElement(56, omekaItem.element_texts),
          source: findElement(48, omekaItem.element_texts),
          extent: '',
          subject: findElement(49, omekaItem.element_texts),
          languageIdentifier: 'eng',
        },
      ],
    };
  }
}
