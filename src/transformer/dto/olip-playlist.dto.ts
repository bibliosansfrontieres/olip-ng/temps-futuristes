import { IOmekaPackage } from 'src/omeka/interfaces/omeka/omeka-package.interface';
import { IOlipPlaylist } from '../interfaces/olip-playlist.interface';
import { OlipPlaylistTranslationDto } from './olip-playlist-translation.dto';

export class OlipPlaylistDto implements IOlipPlaylist {
  id: string;
  contentId: string;
  image: string;
  updatedAt: string;
  playlistTranslations: OlipPlaylistTranslationDto[];
  categoryIds: number[];
  itemIds: number[];
  size: number;
  applicationId: string;

  // TODO : transform to this :
  //playlistTranslations: IPlaylistTranslation[];
  //items: IItem[];

  constructor(omekaPackage: IOmekaPackage, categoryIds: number[]) {
    this.id = omekaPackage.id.toString();
    // TODO: No image in omekapackage....
    this.image = 'images/';
    this.updatedAt = omekaPackage.last_exportable_modification;
    this.playlistTranslations = [
      new OlipPlaylistTranslationDto(omekaPackage, 'eng'),
    ];
    this.categoryIds = categoryIds;
    this.itemIds = omekaPackage.contents.map((content) => content.item_id);
    this.contentId = omekaPackage.id.toString();
    this.size = 0;
    this.applicationId = 'olip';
  }
}
