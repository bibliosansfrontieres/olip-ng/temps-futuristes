import { IOmekaPackage } from 'src/omeka/interfaces/omeka/omeka-package.interface';

export class OlipPlaylistTranslationDto {
  title: string;
  description: string;
  languageIdentifier: string;

  constructor(omekaPackage: IOmekaPackage, languageIdentifier: string) {
    this.title = omekaPackage.ideascube_name;
    this.description = omekaPackage.description;
    this.languageIdentifier = languageIdentifier;
  }
}
