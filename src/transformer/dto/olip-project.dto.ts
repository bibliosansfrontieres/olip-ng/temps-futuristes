import { IOmekaItem } from 'src/omeka/interfaces/omeka/omeka-item.interface';
import { IOlipProject } from '../interfaces/olip-project.interface';
import { findElement } from 'src/utils/find-element.util';

export class OlipProjectDto implements IOlipProject {
  id: number;
  url: string;
  description: string;
  createdAt: string;
  updatedAt: string;
  title: string;
  date: string;
  languages: string;
  startDate: string;
  endDate: string;
  partners: string;
  location: string;
  device: string;
  projectManager: string;

  constructor(project: IOmekaItem) {
    this.id = project.id;
    this.url = project.url;
    this.createdAt = project.added;
    this.updatedAt = project.modified;
    this.description = findElement(41, project.element_texts);
    this.title = findElement(50, project.element_texts);
    this.date = findElement(40, project.element_texts);
    this.languages = findElement(44, project.element_texts);
    this.startDate = findElement(259, project.element_texts);
    this.partners = findElement(260, project.element_texts);
    this.location = findElement(261, project.element_texts);
    this.device = findElement(262, project.element_texts);
    this.projectManager = findElement(263, project.element_texts);
    this.endDate = findElement(264, project.element_texts);
  }
}
