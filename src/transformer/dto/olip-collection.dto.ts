import { findElement } from 'src/utils/find-element.util';
import { IOmekaCollection } from 'src/omeka/interfaces/omeka/omeka-collection.interface';

export class OlipCollection {
  id: string;
  title: string;

  constructor(omekaCollection: IOmekaCollection) {
    this.id = omekaCollection.id.toString();
    this.title = findElement(50, omekaCollection.element_texts);
  }
}
