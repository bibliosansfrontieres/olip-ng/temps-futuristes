import { Module, forwardRef } from '@nestjs/common';
import { TransformerService } from './transformer.service';
import { OmekaTransformerService } from './omeka-transformer.service';
import { DublinCoreTypesModule } from 'src/dublin-core-types/dublin-core-types.module';
import { CatalogModule } from 'src/catalog/catalog.module';

@Module({
  imports: [DublinCoreTypesModule, forwardRef(() => CatalogModule)],
  providers: [TransformerService, OmekaTransformerService],
  exports: [TransformerService, OmekaTransformerService],
})
export class TransformerModule {}
