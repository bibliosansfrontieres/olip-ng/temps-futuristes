import { Controller, Get, Param, Query } from '@nestjs/common';
import { ProjectDefinitionsService } from './project-definitions.service';

@Controller('project-definitions')
export class ProjectDefinitionsController {
  constructor(
    private readonly projectDefinitionsService: ProjectDefinitionsService,
  ) {}

  @Get(':omekaProjectId')
  getProjectDefinition(
    @Param('omekaProjectId') omekaProjectId: string,
    @Query('cache') cache: boolean = true,
  ) {
    return this.projectDefinitionsService.getProjectDefinition(
      +omekaProjectId,
      cache,
    );
  }
}
