import { Injectable, Logger } from '@nestjs/common';
import { existsSync, readFileSync } from 'fs';
import { join } from 'path';
import { ProjectErrorsDto } from 'src/omeka/dto/project-errors.dto';
import { OmekaCheckerService } from 'src/omeka/omeka-checker.service';
import { OmekaProjectDefinitionsService } from 'src/omeka/omeka-project-definitions.service';
import { OmekaService } from 'src/omeka/omeka.service';
import { IOlipProjectDefinition } from 'src/transformer/interfaces/olip-project-definition.interface';
import { TransformerService } from 'src/transformer/transformer.service';

@Injectable()
export class ProjectDefinitionsService {
  constructor(
    private omekaProjectDefinitionsService: OmekaProjectDefinitionsService,
    private omekaCheckerService: OmekaCheckerService,
    private transformerService: TransformerService,
  ) {}

  async getProjectDefinition(
    omekaProjectId: number,
    cache: boolean = true,
  ): Promise<IOlipProjectDefinition | ProjectErrorsDto> {
    if (cache === true) {
      const projetDefinitionPath = join(
        __dirname,
        '../../data/project-definitions',
        `${omekaProjectId}.json`,
      );
      const isProjectDefinitionExists = existsSync(projetDefinitionPath);
      if (isProjectDefinitionExists) {
        const projectDefinition = readFileSync(projetDefinitionPath).toString();
        const project = JSON.parse(projectDefinition);

        let total = 0;
        for (const playlist of project.olipPlaylists) {
          console.log(playlist.itemIds.length);
          total += playlist.itemIds.length;
        }
        console.log(total);
        console.log(project.olipItems.length);
        return project;
      }
    }
    Logger.log(
      `[${omekaProjectId}] - Start creating project definition...`,
      'ProjectDefinitionService',
    );
    const fullProject =
      await this.omekaProjectDefinitionsService.getProject(omekaProjectId);

    Logger.log(
      `[${omekaProjectId}] - Errors checking 1/1 - Checking errors in Omeka data...`,
      'ProjectDefinitionService',
    );
    const errors = await this.omekaCheckerService.checkFullProject(fullProject);

    if (errors.count > 0) {
      Logger.error(
        `[${omekaProjectId}] - Errors checking 1/1 - ${errors.count} errors found !`,
        'ProjectDefinitionService',
      );
      return errors;
    }
    Logger.log(
      `[${omekaProjectId}] - Errors checking 1/1 - Done !`,
      'ProjectDefinitionService',
    );

    Logger.log(
      `[${omekaProjectId}] - Tranformer 1/1 - Transform data from Omeka to OLIP v2...`,
      'ProjectDefinitionService',
    );
    const project =
      await this.transformerService.transformFromOmeka(fullProject);

    Logger.log(
      `[${omekaProjectId}] - Project definition successfully created !`,
      'ProjectDefinitionService',
    );
    let total = 0;
    for (const playlist of project.olipPlaylists) {
      console.log(playlist.itemIds.length);
      total += playlist.itemIds.length;
    }
    console.log(total);
    return project;
  }
}
