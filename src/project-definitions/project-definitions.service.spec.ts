import { Test, TestingModule } from '@nestjs/testing';
import { ProjectDefinitionsService } from './project-definitions.service';

describe('ProjectDefinitionsService', () => {
  let service: ProjectDefinitionsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ProjectDefinitionsService],
    }).compile();

    service = module.get<ProjectDefinitionsService>(ProjectDefinitionsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
