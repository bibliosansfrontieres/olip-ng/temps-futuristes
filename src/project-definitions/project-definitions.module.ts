import { Module } from '@nestjs/common';
import { ProjectDefinitionsService } from './project-definitions.service';
import { ProjectDefinitionsController } from './project-definitions.controller';
import { OmekaModule } from 'src/omeka/omeka.module';
import { TransformerModule } from 'src/transformer/transformer.module';

@Module({
  imports: [OmekaModule, TransformerModule],
  controllers: [ProjectDefinitionsController],
  providers: [ProjectDefinitionsService],
  exports: [ProjectDefinitionsService],
})
export class ProjectDefinitionsModule {}
