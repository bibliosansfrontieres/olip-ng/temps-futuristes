import { Test, TestingModule } from '@nestjs/testing';
import { ProjectDefinitionsController } from './project-definitions.controller';
import { ProjectDefinitionsService } from './project-definitions.service';

describe('ProjectDefinitionsController', () => {
  let controller: ProjectDefinitionsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ProjectDefinitionsController],
      providers: [ProjectDefinitionsService],
    }).compile();

    controller = module.get<ProjectDefinitionsController>(ProjectDefinitionsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
