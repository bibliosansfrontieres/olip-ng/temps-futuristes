import { Module } from '@nestjs/common';
import { InstancesService } from './instances.service';
import { InstancesController } from './instances.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Instance } from './entities/instance.entity';
import { DockerModule } from 'src/docker/docker.module';
import { ProjectDefinitionsModule } from 'src/project-definitions/project-definitions.module';
import { InstanceError } from './entities/instance-error.entity';
import { InstanceProject } from './entities/instance-project.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([Instance, InstanceError, InstanceProject]),
    DockerModule,
    ProjectDefinitionsModule,
  ],
  controllers: [InstancesController],
  providers: [InstancesService],
})
export class InstancesModule {}
