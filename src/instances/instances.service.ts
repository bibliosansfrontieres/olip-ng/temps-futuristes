import {
  Injectable,
  InternalServerErrorException,
  Logger,
  NotFoundException,
} from '@nestjs/common';
import { CreateInstanceDto } from './dto/create-instance.dto';
import { Instance } from './entities/instance.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { join } from 'path';
import { mkdirSync, readFileSync, rmSync, writeFileSync } from 'fs';
import { copySync } from 'fs-extra';
import { DockerService } from 'src/docker/docker.service';
import * as dotenv from 'dotenv';
import { ConfigService } from '@nestjs/config';
import { InstanceStatusEnum } from './enums/instance-status.enum';
import { ProjectDefinitionsService } from 'src/project-definitions/project-definitions.service';
import { ProjectErrorsDto } from 'src/omeka/dto/project-errors.dto';
import { InstanceError } from './entities/instance-error.entity';
import { InstanceProject } from './entities/instance-project.entity';
import { generateVmId } from 'src/utils/generate-vm-id.util';
import { Response } from 'express';

@Injectable()
export class InstancesService {
  private instancesFolderPath: string;
  private exampleInstancePath: string;
  constructor(
    @InjectRepository(Instance)
    private instancesRepository: Repository<Instance>,
    @InjectRepository(InstanceError)
    private instanceErrorsRepository: Repository<InstanceError>,
    @InjectRepository(InstanceProject)
    private instanceProjectsRepository: Repository<InstanceProject>,
    private dockerService: DockerService,
    private configService: ConfigService,
    private projectDefinitionsService: ProjectDefinitionsService,
  ) {
    this.instancesFolderPath = join(__dirname, '../../data/instances');
    this.exampleInstancePath = join(__dirname, '../../data/template');
  }

  async onModuleInit() {
    const instances = await this.instancesRepository.find({
      where: { status: InstanceStatusEnum.UP },
    });
    Logger.log(`Starting ${instances.length} instances...`, 'InstancesService');
    const instancesStart = [];
    for (const instance of instances) {
      instancesStart.push(this.dockerService.runInstance(instance));
    }
    await Promise.all(instancesStart);
    Logger.log(`${instances.length} instances started !`, 'InstancesService');
  }

  async onModuleDestroy() {
    const instances = await this.instancesRepository.find({
      where: { status: InstanceStatusEnum.UP },
    });
    Logger.log(`Stopping ${instances.length} instances...`, 'InstancesService');
    const instancesStop = [];
    for (const instance of instances) {
      instancesStop.push(this.dockerService.stopInstance(instance));
    }
    await Promise.all(instancesStop);
    Logger.log(`${instances.length} instances stopped !`, 'InstancesService');
  }

  async generateVmId(): Promise<string> {
    let vmId = generateVmId();
    let foundVmId = await this.instancesRepository.findOne({ where: { vmId } });
    while (foundVmId !== null) {
      vmId = generateVmId();
      foundVmId = await this.instancesRepository.findOne({ where: { vmId } });
    }
    return vmId;
  }

  async create(createInstanceDto: CreateInstanceDto) {
    const vmId = await this.generateVmId();
    let instance = new Instance(createInstanceDto, vmId);
    instance = await this.instancesRepository.save(instance);
    this.verifyInstance(instance);
    return instance;
  }

  async shutdown(vmId: string) {
    const instance = await this.instancesRepository.findOne({
      where: { vmId },
    });
    if (instance === null)
      throw new NotFoundException(`instance ${vmId} not found`);
    await this.dockerService.stopInstance(instance);
    instance.status = InstanceStatusEnum.DOWN;
    await this.instancesRepository.save(instance);
  }

  async start(vmId: string) {
    const instance = await this.instancesRepository.findOne({
      where: { vmId },
    });
    if (instance === null)
      throw new NotFoundException(`instance ${vmId} not found`);
    await this.dockerService.runInstance(instance);
    instance.status = InstanceStatusEnum.UP;
    await this.instancesRepository.save(instance);
  }

  async getProjectDefinition(instance: Instance) {
    try {
      const projectDefinition =
        await this.projectDefinitionsService.getProjectDefinition(
          instance.omekaProjectId,
          false,
        );
      const instanceProject = new InstanceProject(
        instance,
        projectDefinition.olipProject,
      );
      await this.instanceProjectsRepository.save(instanceProject);
      return projectDefinition;
    } catch (e) {
      console.error(e);
      return this.getProjectDefinition(instance);
    }
  }

  async verifyInstance(instance: Instance) {
    const projectDefinition = await this.getProjectDefinition(instance);
    if (projectDefinition instanceof ProjectErrorsDto) {
      for (const error of projectDefinition.itemsErrors) {
        const instanceError = new InstanceError(instance, error);
        await this.instanceErrorsRepository.save(instanceError);
      }
      instance.status = InstanceStatusEnum.ERROR;
      await this.instancesRepository.save(instance);
    } else {
      await this.instanceErrorsRepository.delete({
        instance: { id: instance.id },
      });
      await this.copyExampleInstance(instance);
      instance.status = InstanceStatusEnum.VALID;
      await this.instancesRepository.save(instance);
      const result = await this.dockerService.runInstance(instance);
      if (result.raw !== '') {
        throw new InternalServerErrorException(
          `could not start instance ${instance.vmId}`,
        );
      } else {
        instance.status = InstanceStatusEnum.INSTALLING;
        await this.instancesRepository.save(instance);
        while (instance.status === InstanceStatusEnum.INSTALLING) {
          try {
            const isDeployedResult = await fetch(
              `http://olip-${instance.vmId}/api/maestro/status`,
            );
            if (isDeployedResult.status === 200) {
              const data = await isDeployedResult.json();
              if (data.isDeployed === true) {
                instance.status = InstanceStatusEnum.UP;
                await this.instancesRepository.save(instance);
              }
            }
          } catch (e) {
            console.error(e);
          }
          await new Promise((resolve) => setTimeout(resolve, 5000));
        }
      }
    }
  }

  async copyExampleInstance(instance: Instance) {
    const instanceFolderPath = join(
      this.instancesFolderPath,
      `${instance.vmId}`,
    );
    mkdirSync(instanceFolderPath);
    const sourceDir = this.exampleInstancePath;
    const destDir = instanceFolderPath;
    copySync(sourceDir, destDir);
    await this.configureEnvVariables(instance, destDir);
  }

  async configureEnvVariables(instance: Instance, destDir: string) {
    // Docker
    const dockerEnvFilePath = join(destDir, '.env');
    const dockerEnvFileContent = readFileSync(dockerEnvFilePath, 'utf8');
    const parsedDockerEnv = dotenv.parse(dockerEnvFileContent);

    parsedDockerEnv.OLIP_HOST = `${instance.vmId}.${this.configService.get(
      'MAESTRO_URL',
    )}`;
    parsedDockerEnv.TLS_ENABLED =
      this.configService.get('TLS_ENABLED') || 'false';
    parsedDockerEnv.SHARED_FOLDER = `${this.configService.get(
      'SHARED_FOLDER',
    )}/instances/${instance.vmId}`;

    // Optional vars
    parsedDockerEnv.OLIP_IMAGE = 'offlineinternet/olip-api-v2:maestro';
    parsedDockerEnv.VM_ID = `${instance.vmId}`;
    parsedDockerEnv.OLIP_NETWORK = `${this.configService.get('OLIP_NETWORK')}`;
    parsedDockerEnv.OLIP_NETWORK_EXTERNAL = `true`;

    const updatedDockerEnvContent = Object.keys(parsedDockerEnv)
      .map((key) => `${key}=${parsedDockerEnv[key]}`)
      .join('\n');
    writeFileSync(dockerEnvFilePath, updatedDockerEnvContent);

    // HAO
    /*
    const haoEnvFilePath = join(destDir, '.hao.env');
    const haoEnvFileContent = readFileSync(haoEnvFilePath, 'utf8');
    const parsedHaoEnv = dotenv.parse(haoEnvFileContent);

    const updatedHaoEnvContent = Object.keys(parsedHaoEnv)
      .map((key) => `${key}=${parsedHaoEnv[key]}`)
      .join('\n');
    writeFileSync(haoEnvFilePath, updatedHaoEnvContent);
    */

    // OLIP

    const olipEnvFilePath = join(destDir, '.olip.env');
    const olipEnvFileContent = readFileSync(olipEnvFilePath, 'utf8');
    const parsedOlipEnv = dotenv.parse(olipEnvFileContent);

    parsedOlipEnv.HAO_URL = `http://hao-${instance.vmId}`;
    parsedOlipEnv.MAESTRO_URL = `${this.configService.get(
      'MAESTRO_URL_FOR_OLIP',
    )}`;

    // Optional vars
    parsedOlipEnv.OMEKA_PROJECT_ID = `${instance.omekaProjectId}`;
    parsedOlipEnv.VM_ID = `${instance.vmId}`;
    parsedOlipEnv.OLIP_HOST = `${instance.vmId}.${this.configService.get(
      'MAESTRO_URL',
    )}`;
    parsedOlipEnv.SHARED_APPLICATIONS_FOLDER = `${this.configService.get(
      'SHARED_FOLDER',
    )}/instances/${instance.vmId}/applications`;
    parsedOlipEnv.APPLICATIONS_NETWORK = `${this.configService.get(
      'OLIP_NETWORK',
    )}`;
    parsedOlipEnv.TLS_ENABLED = `${this.configService.get('TLS_ENABLED')}`;

    const updatedOlipEnvContent = Object.keys(parsedOlipEnv)
      .map((key) => `${key}=${parsedOlipEnv[key]}`)
      .join('\n');
    writeFileSync(olipEnvFilePath, updatedOlipEnvContent);
  }

  async delete(id: number) {
    const instance = await this.instancesRepository.findOne({ where: { id } });
    if (instance === null)
      throw new NotFoundException(`instance ${id} not found`);
    const result = await this.dockerService.stopInstance(instance);
    this.deleteInstance(instance);
    await this.instancesRepository.remove(instance);
  }

  async deleteInstance(instance: Instance) {
    rmSync(join(this.instancesFolderPath, `${instance.vmId}`), {
      recursive: true,
    });
  }

  findAll() {
    return this.instancesRepository.find({ order: { id: 'DESC' } });
  }

  async getAll() {
    const instances = await this.findAll();
    return instances.map((instance) => {
      const url = `http://${instance.vmId}.${this.configService.get(
        'MAESTRO_URL',
      )}`;
      return {
        ...instance,
        url,
      };
    });
  }

  async get(vmId: string) {
    const instance = await this.instancesRepository.findOne({
      where: { vmId },
      relations: { instanceErrors: true, instanceProjects: true },
    });
    if (instance === null)
      throw new NotFoundException(`instance ${vmId} not found`);
    const url = `http://${instance.vmId}.${this.configService.get(
      'MAESTRO_URL',
    )}`;
    return { url, ...instance };
  }

  getLogsStream(response: Response, vmId: string) {
    return this.dockerService.getLogsStream(response, vmId);
  }

  getLogs(vmId: string) {
    return this.dockerService.getLogs(vmId);
  }

  async getErrors(instanceId: number) {
    const instance = await this.instancesRepository.findOne({
      where: { id: instanceId },
      relations: { instanceErrors: true },
    });
    if (instance === null)
      throw new NotFoundException(`instance ${instanceId} not found`);
    return instance.instanceErrors;
  }
}
