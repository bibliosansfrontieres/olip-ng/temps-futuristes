import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { CreateInstanceDto } from '../dto/create-instance.dto';
import * as uuid from 'uuid';
import { InstanceStatusEnum } from '../enums/instance-status.enum';
import { InstanceError } from './instance-error.entity';
import { InstanceProject } from './instance-project.entity';
import { generateVmId } from 'src/utils/generate-vm-id.util';

@Entity()
export class Instance {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  vmId: string;

  @Column()
  omekaProjectId: number;

  @Column()
  creatorMail: string;

  @Column()
  creatorName: string;

  @Column()
  status: InstanceStatusEnum;

  @OneToMany(() => InstanceError, (instanceError) => instanceError.instance)
  instanceErrors: InstanceError[];

  @OneToMany(
    () => InstanceProject,
    (instanceProject) => instanceProject.instance,
  )
  instanceProjects: InstanceProject[];

  constructor(createInstanceDto: CreateInstanceDto, vmId: string) {
    if (!createInstanceDto) return;
    this.omekaProjectId = createInstanceDto.omekaProjectId;
    this.creatorMail = createInstanceDto.creatorMail;
    this.creatorName = createInstanceDto.creatorName;
    this.status = InstanceStatusEnum.QUEUED;
    this.vmId = vmId;
  }
}
