import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Instance } from './instance.entity';
import { ErrorDto } from 'src/omeka/dto/error.dto';

@Entity()
export class InstanceError {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  message: string;

  @Column({ nullable: true })
  url: string;

  @ManyToOne(() => Instance, (instance) => instance.instanceErrors)
  instance: Instance;

  constructor(instance: Instance, error: ErrorDto) {
    if (!instance) return;
    this.instance = instance;
    this.message = `${error.type} ID ${error.id}: ${error.message}`;
    if (error.type !== 'error') {
      if (error.type === 'project') {
        this.url = `https://omeka.tm.bsf-intranet.org/admin/items/show/${error.id}`;
      }
      if (error.type === 'item') {
        this.url = `https://omeka.tm.bsf-intranet.org/admin/items/show/${error.id}`;
      }
      if (error.type === 'package') {
        this.url = `https://omeka.tm.bsf-intranet.org/admin/package-manager/index/show/id/${error.id}`;
      }
    }
  }
}
