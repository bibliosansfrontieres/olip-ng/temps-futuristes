import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Instance } from './instance.entity';
import { IOlipProject } from 'src/transformer/interfaces/olip-project.interface';

@Entity()
export class InstanceProject {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  url: string;

  @Column()
  createdAt: string;

  @Column()
  updatedAt: string;

  @Column()
  title: string;

  @Column()
  date: string;

  @Column()
  languages: string;

  @Column()
  startDate: string;

  @Column()
  endDate: string;

  @Column()
  partners: string;

  @Column()
  location: string;

  @Column()
  device: string;

  @Column()
  projectManager: string;

  @ManyToOne(() => Instance, (instance) => instance.instanceProjects)
  instance: Instance;

  constructor(instance: Instance, olipProject: IOlipProject) {
    if (!instance || !olipProject) return;
    this.instance = instance;
    this.url = olipProject.url;
    this.createdAt = olipProject.createdAt;
    this.updatedAt = olipProject.updatedAt;
    this.title = olipProject.title;
    this.date = olipProject.date;
    this.languages = olipProject.languages;
    this.startDate = olipProject.startDate;
    this.endDate = olipProject.endDate;
    this.partners = olipProject.partners;
    this.location = olipProject.location;
    this.device = olipProject.device;
    this.projectManager = olipProject.projectManager;
  }
}
