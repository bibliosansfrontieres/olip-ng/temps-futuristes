export enum InstanceStatusEnum {
  QUEUED = 'queued',
  ERROR = 'error',
  VALID = 'valid',
  INSTALLING = 'installing',
  UP = 'up',
  DOWN = 'down',
}
