import {
  Controller,
  Post,
  Body,
  Delete,
  Param,
  Get,
  Res,
} from '@nestjs/common';
import { InstancesService } from './instances.service';
import { CreateInstanceDto } from './dto/create-instance.dto';
import { Auth } from 'src/auth/auth.decorator';
import { Response } from 'express';

@Controller('instances')
export class InstancesController {
  constructor(private readonly instancesService: InstancesService) {}

  @Get()
  getAll() {
    return this.instancesService.getAll();
  }

  @Get(':vmId')
  get(@Param('vmId') vmId: string) {
    return this.instancesService.get(vmId);
  }

  @Get(':vmId/logs')
  getLogs(@Param('vmId') vmId: string) {
    return this.instancesService.getLogs(vmId);
  }

  @Get(':vmId/stream')
  getLogsStream(@Param('vmId') vmId: string, @Res() response: Response) {
    return this.instancesService.getLogsStream(response, vmId);
  }

  @Get(':id/errors')
  getErrors(@Param('id') id: string) {
    return this.instancesService.getErrors(+id);
  }

  @Post()
  create(@Body() createInstanceDto: CreateInstanceDto) {
    return this.instancesService.create(createInstanceDto);
  }

  @Post(':vmId/shutdown')
  shutdown(@Param('vmId') vmId: string) {
    return this.instancesService.shutdown(vmId);
  }

  @Post(':vmId/start')
  start(@Param('vmId') vmId: string) {
    return this.instancesService.start(vmId);
  }

  @Delete(':id')
  delete(@Param('id') id: string) {
    return this.instancesService.delete(+id);
  }
}
