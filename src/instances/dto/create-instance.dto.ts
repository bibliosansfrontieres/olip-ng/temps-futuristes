import { IsNumber, IsString } from 'class-validator';

export class CreateInstanceDto {
  @IsNumber()
  omekaProjectId: number;

  @IsString()
  creatorMail: string;

  @IsString()
  creatorName: string;
}
