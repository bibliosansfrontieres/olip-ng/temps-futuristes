import { Injectable } from '@nestjs/common';
import { CreateDublinCoreTypeTranslationDto } from 'src/dublin-core-types/dto/create-dublin-core-type-translation.dto';
import { CreateDublinCoreTypeDto } from 'src/dublin-core-types/dto/create-dublin-core-type.dto';
import { DublinCoreTypesService } from 'src/dublin-core-types/dublin-core-types.service';
import { OmekaService } from 'src/omeka/omeka.service';

@Injectable()
export class InitDatabaseService {
  private defaultLanguage = 'eng';

  constructor(
    private omekaService: OmekaService,
    private dublinCoreTypesService: DublinCoreTypesService,
  ) {}

  async onModuleInit() {
    await this.createDublinCoreTypes();
  }

  async createDublinCoreTypes() {
    const count = await this.dublinCoreTypesService.count();
    if (count > 0) return;
    const omekaItemTypes = await this.omekaService.getOmekaItemTypes();
    for (const omekaItemType of omekaItemTypes) {
      const createDublinCoreTypeDto: CreateDublinCoreTypeDto = {
        image: 'images/',
      };
      const dublinCoreType = await this.dublinCoreTypesService.create(
        createDublinCoreTypeDto,
      );
      const createDublinCoreTypeTranslationDto: CreateDublinCoreTypeTranslationDto =
        {
          label: omekaItemType.name,
          description: omekaItemType.description,
          languageIdentifier: this.defaultLanguage,
        };
      await this.dublinCoreTypesService.createTranslation(
        createDublinCoreTypeTranslationDto,
        dublinCoreType,
      );
    }
  }
}
