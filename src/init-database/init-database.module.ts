import { Module } from '@nestjs/common';
import { InitDatabaseService } from './init-database.service';
import { OmekaModule } from 'src/omeka/omeka.module';
import { DublinCoreTypesModule } from 'src/dublin-core-types/dublin-core-types.module';

@Module({
  imports: [OmekaModule, DublinCoreTypesModule],
  providers: [InitDatabaseService],
})
export class InitDatabaseModule {}
