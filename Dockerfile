# Use a smaller base image
FROM node:18-alpine AS build

WORKDIR /app

COPY package.json package-lock.json ./

RUN npm ci

COPY . .

RUN npm run build

# Production Stage
FROM node:18-alpine

WORKDIR /app

RUN apk update && apk add docker docker-compose --no-cache

COPY --from=build /app/node_modules /app/node_modules
COPY --from=build /app/dist /app/dist
COPY --from=build /app/data /app/data

ENV NODE_ENV production

CMD [ "node", "dist/main.js" ]