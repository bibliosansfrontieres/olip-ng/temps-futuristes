function transformCatalog(applicationCatalog) {
  // Retrieve contents from application catalog
  const contents = applicationCatalog.feed.entry;

  const applicationContents = [];

  for (const content of contents) {
    // Retrieve informations of content
    const url = content.link[2]['@_href'].replace('.meta4', '');
    const nameSplit = url.split('/');
    const name = nameSplit[nameSplit.length - 1];
    const idSplit = content.id.split(':');
    const id = idSplit[idSplit.length - 1];

    // Format content informations for OLIP
    const applicationContent = {
      id,
      version: content.updated,
      description: content.summary,
      languageIdentifier: content.language,
      title: content.title,
      url,
      thumbnail: 'https://library.kiwix.org' + content.link[0]['@_href'],
      size: parseInt(content.link[2]['@_length'] || '0'),
      name,
      mimeType: content.link[2]['@_type'],
    };

    // Add formated content to array
    applicationContents.push(applicationContent);
  }

  // Return formated contents
  return applicationContents;
}

module.exports = { transformCatalog };
